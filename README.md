## Installation


#### Installeer packages

```
composer install
```


#### Hernoem **.env.example** naar **.env**

> Vergeet niet om de database gegevens te wijzigen in de .env
```
// windows
ren .env.example .env

// linux
mv .env.example .env
```



#### Voer de volgende commands uit

```
// Maakt een applicate key aan in de .env
php artisan key:generate

// voert alle migraties uit om de database te vullen met tables
php artisan migrate

// verwijder alle configuratie cache (optioneel)
php artisan config:clear

// start een PHP server (optioneel)
php artisan serve
```

> Voor meer info bekijk [Laravel installatie](https://laravel.com/docs/master)