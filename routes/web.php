<?php

use App\Http\Controllers\ActionController;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\DraftController;
use App\Http\Controllers\EditionController;
use App\Http\Controllers\FAQController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\TrashController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web rout   es for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::group(['prefix' => 'login'], function () {
    Route::get('/microsoft', [LoginController::class, 'redirectToProvider']);
    Route::get('/microsoft/callback', [LoginController::class, 'handleProviderCallback']);
});


Route::group(['prefix' => 'activity'], function () {
    Route::post('/participate/{id}', [ActionController::class, 'participate'])->name('public.activity.participate');
    Route::post('/retire/{id}', [ActionController::class, 'retire'])->name('public.activity.retire');

    Route::post('/favorite/{id}', [ActionController::class, 'favorite'])->name('public.activity.favorite');
    Route::post('/unfavorite/{id}', [ActionController::class, 'unfavorite'])->name('public.activity.unfavorite');
});


Route::group([
    'middleware' => ['auth:sanctum', 'verified', 'is_due'],
    'prefix' => 'admin/'
], function () {
    Route::get('/dashboard', [PagesController::class, 'adminDashboard'])->name('dashboard');

    Route::group(['prefix' => 'activity'], function () {
        Route::get('/show/{activity}', [ActivityController::class, 'show']);
        Route::get('/list', [ActivityController::class, 'index'])->name('pages.activity.list');

        Route::get('/edit/{id}', [ActivityController::class, 'edit'])->name('pages.activity.edit');
        Route::put('/update/{activity}', [ActivityController::class, 'update'])->name('pages.activity.update');
    });

    Route::get('/participants/{id}', [ProgramController::class, 'listParticipants'])->name('pages.participant.list');
    Route::get('/participants/edit/{id}', [ProgramController::class, 'editParticipants'])->name('pages.participant.edit');
    Route::post('/participants/add', [ProgramController::class, 'addParticipants'])->name('pages.participant.add');
    Route::post('/participants/remove', [ProgramController::class, 'retireParticipant'])->name('pages.participant.retire');
    Route::get('/program', [ProgramController::class, 'listProgram'])->name('pages.program.list');
    Route::get('/favorite', [ProgramController::class, 'listFavorite'])->name('pages.program.favorite');

    Route::group(['prefix' => 'draft'], function () {
        Route::get('/show/{activity}', [DraftController::class, 'show']);
        Route::get('/list', [DraftController::class, 'index'])->name('pages.draft.list');

        Route::get('/edit/{id}', [DraftController::class, 'edit'])->name('pages.draft.edit');
        Route::get('/create', [DraftController::class, 'create'])->name('pages.draft.create');
        Route::post('/store', [DraftController::class, 'store'])->name('pages.draft.store');
        Route::put('/update/{activity}', [DraftController::class, 'update'])->name('pages.draft.update');
    });

    Route::group([
        'middleware' => 'is_admin'
    ], function () {
        Route::group(['prefix' => 'activity'], function () {
            Route::delete('/delete/{activity}', [ActivityController::class, 'destroy'])->name('pages.activity.delete');
        });

        Route::get('auth/register', [LoginController::class, 'viewRegister'])->name('auth.register.view');
        Route::post('auth/register', [LoginController::class, 'register'])->name('auth.register');

        Route::group(['prefix' => 'location'], function () {
            Route::get('/list', [LocationController::class, 'index'])->name('pages.location.list');

            Route::get('/edit/{id}', [LocationController::class, 'edit'])->name('pages.location.edit');
            Route::get('/create', [LocationController::class, 'create'])->name('pages.location.create');

            Route::post('/store', [LocationController::class, 'store'])->name('pages.location.store');
            Route::put('/update/{location}', [LocationController::class, 'update'])->name('pages.location.update');

            Route::delete('/delete/{location}', [LocationController::class, 'destroy'])->name('pages.location.delete');
        });

        Route::group(['prefix' => 'tag'], function () {
            Route::get('/list', [TagController::class, 'index'])->name('pages.tag.list');

            Route::get('/edit/{id}', [TagController::class, 'edit'])->name('pages.tag.edit');
            Route::get('/create', [TagController::class, 'create'])->name('pages.tag.create');

            Route::post('/store', [TagController::class, 'store'])->name('pages.tag.store');
            Route::put('/update/{tag}', [TagController::class, 'update'])->name('pages.tag.update');

            Route::delete('/delete/{tag}', [TagController::class, 'destroy'])->name('pages.tag.delete');
        });

        Route::group(['prefix' => 'faq'], function () {
            Route::get('/list', [FAQController::class, 'index'])->name('pages.faq.list');

            Route::get('/edit/{id}', [FAQController::class, 'edit'])->name('pages.faq.edit');
            Route::get('/create', [FAQController::class, 'create'])->name('pages.faq.create');

            Route::post('/store', [FAQController::class, 'store'])->name('pages.faq.store');
            Route::put('/update/{faq}', [FAQController::class, 'update'])->name('pages.faq.update');

            Route::delete('/delete/{faq}', [FAQController::class, 'destroy'])->name('pages.faq.delete');
        });

        Route::group(['prefix' => 'edition'], function () {
            Route::get('/list', [EditionController::class, 'index'])->name('pages.edition.list');

            Route::get('/edit/{edition}', [EditionController::class, 'edit'])->name('pages.edition.edit');
            Route::put('/update/{edition}', [EditionController::class, 'update'])->name('pages.edition.update');

            Route::delete('/delete/{edition}', [EditionController::class, 'destroy'])->name('pages.edition.delete');
        });

        Route::group(['prefix' => 'trash'], function () {
            Route::get('/list', [TrashController::class, 'index'])->name('pages.trash.list');
            Route::put('/update/{id}', [TrashController::class, 'update'])->name('pages.trash.restore');
            Route::delete('/delete/{id}', [TrashController::class, 'destroy'])->name('pages.trash.delete');
        });

        Route::group(['prefix' => 'user'], function () {
            Route::get('/show/{user}', [UserController::class, 'show']);
            Route::get('/list', [UserController::class, 'index'])->name('pages.user.list');

            Route::get('/edit/{user}', [UserController::class, 'edit'])->name('pages.user.edit');
            Route::get('/create', [UserController::class, 'create'])->name('pages.trash.user');

            Route::post('/store', [UserController::class, 'store'])->name('pages.user.store');
            Route::put('/update/{user}', [UserController::class, 'update'])->name('pages.user.update');

            Route::delete('/delete/{user}', [UserController::class, 'destroy'])->name('pages.user.delete');
        });

        Route::group(['prefix' => 'content'], function () {
            Route::get('/show/{content}', [ContentController::class, 'show']);
            Route::get('/list', [ContentController::class, 'index'])->name('pages.content.list');

            Route::get('/edit/{content}', [ContentController::class, 'edit'])->name('pages.content.edit');
            Route::get('/create', [ContentController::class, 'create'])->name('pages.content.create');

            Route::post('/store', [ContentController::class, 'store'])->name('pages.content.store');
            Route::put('/update/{content}', [ContentController::class, 'update'])->name('pages.content.update');

            Route::delete('/delete/{content}', [ContentController::class, 'destroy'])->name('pages.content.delete');
        });
    });
});

Route::group([
    'middleware' => ['auth:sanctum', 'verified', 'set_language'],
    'prefix' => '{language}/public/'
], function () {
    Route::get('/dashboard', [PagesController::class, 'publicDashboard'])->name('public.dashboard');

    Route::get('/faq', [PagesController::class, 'getFaq'])->name('public.faq');

    Route::group(['prefix' => 'activity'], function () {
        Route::get('/list', [PagesController::class, 'getActivityList'])->name('public.activity.list');
        Route::get('/calender', [PagesController::class, 'getActivityCalender'])->name('public.activity.calender');
    });

    Route::get('/about-think-fest', [PagesController::class, 'getAboutThinkFest'])->name('public.about-think-fest');
    Route::get('/program', [PagesController::class, 'getProgram'])->name('public.program');
    Route::get('/toolkit', [PagesController::class, 'getPressKit'])->name('public.press-kit');
    Route::get('/contact', [PagesController::class, 'getContact'])->name('public.contact');
    Route::get('/privacy-statement', [PagesController::class, 'getPrivacyStatement'])->name('public.privacy-statement');
    Route::get('/cookies', [PagesController::class, 'getCookies'])->name('public.cookies');
    Route::get('/ednext', [PagesController::class, 'getEdNext'])->name('public.ednext');
});
