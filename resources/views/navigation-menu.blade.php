<nav class="ty-nav ty-nav-admin">
    <div class="container">
        <div class="navbar navbar-light navbar-expand-xl bg-transparent">
            <div class="ty-nav-container bg-white rounded">
                <div class="ty-nav-left">
                    <a href="{{ route('public.dashboard', 'nl') }}"><img src="{{ url('images/logo.svg') }}"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarItems" aria-controls="navbarItems" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbarItems">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.draft.list')) active @endif" aria-current="page" href="{{ route('pages.draft.list') }}">Concepts</a>
                        </li>

                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.activity.list')) active @endif" aria-current="page" href="{{ route('pages.activity.list') }}">All Activities</a>
                        </li>

                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.program.list')) active @endif" aria-current="page" href="{{ route('pages.program.list') }}">My Programme</a>
                        </li>

                        @can('viewAny', \App\Models\Edition::class)
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.user.list')) active @endif" aria-current="page" href="{{ route('pages.user.list') }}">Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.faq.list')) active @endif" aria-current="page" href="{{ route('pages.faq.list') }}">FAQ</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.tag.list')) active @endif" aria-current="page" href="{{ route('pages.tag.list') }}">Tags</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.location.list')) active @endif" aria-current="page" href="{{ route('pages.location.list') }}">Locations</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.trash.list')) active @endif" aria-current="page" href="{{ route('pages.trash.list') }}">Recycle bin</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('pages.content.list')) active @endif" aria-current="page" href="{{ route('pages.content.list') }}">Content</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('auth.register.view')) active @endif" aria-current="page" href="{{ route('auth.register.view') }}">Make user</a>
                        </li>
                        @endcan()
                    </ul>
                    <div class="d-flex ty-nav-profile-box">
                        <li class="nav-item dropdown">
                            <a class="ty-nav-profile d-flex align-items-center rounded border nav-link" href="#" id="navbarScrollingDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                                <p>{{ auth()->user()->name }}</p>
                            </a>
                            <ul class="dropdown-menu rounded" aria-labelledby="navbarScrollingDropdown">
                                <li><a class="dropdown-item" href="{{ route('public.dashboard', app()->getLocale()) }}">{{ __('Deelnemen') }}</a></li>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">Uitloggen</a>
                                    </li>
                                </form>
                            </ul>
                        </li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
