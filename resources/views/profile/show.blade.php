<x-guest-layout>
    <div class="container">
        <div class="row g-0">
            <div class="ty-public-title col-lg">
                <h1 class="ty-main-title ty-main-title-db ty-main-title-public">{{ __('Profiel') }}</h1>
            </div>
            <div class="ty-public-content col-lg">
                <div class="ty-dashboard-public ty-dashboard-nav d-flex flex-column">
                    <a href="{{ route('pages.activity.list') }}">Mijn activiteiten</a>
                    {{-- <a href="{{ route('pages.activity.list') }}">Mijn favorieten</a> --}}
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
