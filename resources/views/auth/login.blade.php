<x-guest-layout>
    <div class="container">
        <div class="row">
            <div class="ty-public-title col-lg">
                <h1 class="ty-main-title ty-main-title-db ty-main-title-public">
                    Think Fest<br>
                    4 november<br>
                    2021
                </h1>
            </div>
            <div class="ty-login col-lg">
                <h1 class="ty-main-title">{{ __('Inloggen')}}</h1>

                @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
                @endif

                <x-jet-validation-errors class="mb-4" />
                <form class="ty-login-form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="collapse" id="collapseLogin">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">
                                <img src="{{ url('images/lock.svg') }}">
                            </span>
                            <input id="email" class="form-control" placeholder="E-mail" aria-label="E-mail" aria-describedby="basic-addon1" type="email" name="email" :value="old('email')" required autofocus>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">
                                <img src="{{ url('images/lock.svg') }}">
                            </span>
                            <input id="password" class="form-control" placeholder="Wachtwoord" aria-label="Wachtwoord" aria-describedby="basic-addon1" type="password" name="password" required autocomplete="current-password">
                        </div>
                        <button type="submit" class="btn btn-radius-sm btn-green mb-4">
                            Login
                        </button>
                    </div>

                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <a class="btn btn-radius-sm btn-green-border mr-2" data-bs-toggle="collapse" href="#collapseLogin" role="button" aria-expanded="false" aria-controls="collapseLogin">
                                Gast/Guest
                            </a>
                            <a href="/login/microsoft" class="btn btn-radius-sm btn-green">HHS Account</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
