<x-guest-layout>
    <x-jet-validation-errors class="mb-4" />
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="ty-main-title ty-guest-text">
                    Think Fest<br>
                    4 november<br>
                    2021
                </h2>
            </div>
            <div class="ty-login col-md-5">
                <h1 class="ty-main-title">Login</h1>

                @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1">
                            <img src="{{ url('images/lock.svg') }}">
                        </span>
                        <input id="email" class="form-control" placeholder="E-mail" aria-label="E-mail" aria-describedby="basic-addon1" type="email" name="email" :value="old('email')" required autofocus>
                    </div>

                    <div class="d-flex justify-content-end align-items-center">
                        <div>
                            <a href="/login" class="btn btn-radius-sm btn-green-border mr-2">Terug</a>
                            <button type="submit" class="btn btn-radius-sm btn-green">
                                Verstuur wachtwoord vergeten link
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
