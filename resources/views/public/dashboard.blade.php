<x-guest-layout>
    <div class="container">
        <div class="ty-dashboard ty-dashboard-public row g-0">
            <div class="ty-public-title col-lg">
                <h1 class="ty-main-title ty-main-title-db ty-main-title-public">
                    {{ __('Hi') }}, {{ auth()->user()->name }}</h1>
            </div>
            <div class="ty-public-content col-lg">
                <div class="ty-text-block">
                @if($contents != null)
                    @foreach($contents as $content)
                        @if($content->position == 'header')
                            {!! $content->{'wysiwyg_'.app()->getLocale()} !!}
                            @if($content->image_path != null)
                                <img src="{{ asset('images/activities/' . $content->image_path) }}" alt="Image.broken"></img>
                            @endif()
                            @if($content->{'button_' . app()->getLocale()} != null)
                                <a href="{{ $content->{'link_' . app()->getLocale()} }}" class="btn btn-radius-sm btn-green">{{ $content->{'button_' . app()->getLocale()} }}</a>
                            @endif()
                            
                        @endif()
                    @endforeach()
                @endif()
                </div>
                <a class="ty-scroll-arrow" href="#programme"><img src="{{ url('images/chevron/chevron-down.svg') }}"></a>
            </div>
        </div>

        @include('public/program/view')

        <div class="ty-dashboard-stats ty-stats d-flex flex-wrap justify-content-center align-items-center">
            @foreach($countArray as $key => $value)
            <div class="ty-dashboard-stat d-flex justify-content-center align-items-center flex-column">
                <p class="ty-dashboard-stat-number">{{ $value }}</p>
                <p class="ty-dashboard-stat-cat">{{ __(ucwords($key)) }}</p>
            </div>
            @endforeach()
        </div>

        @if($contents != null)
            @for ($i = 0; $i < count($contents); $i++)
                @if($contents[$i]->position == 'bottom')
                    @if ($i % 2 == 0)
                        <div class="ty-2-col row">
                            <div class="ty-2-col-text col-md offset-md-1">
                                    {!! $contents[$i]->{'wysiwyg_' . app()->getLocale()} !!}
                            </div>
                            <div class="ty-2-col-img col-md">
                                <img src="{{ asset('images/activities/' . $contents[$i]->image_path) }}" alt="Image.broken"></img>
                            </div>
                        </div>
                    @elseif($i % 2 != 0)
                        <div class="ty-2-col ty-2-col-reversed row">
                            <div class="ty-2-col-img col-md-6">
                                <img src="{{ asset('images/activities/' . $contents[$i]->image_path) }}" alt="Image.broken"></img>
                            </div>
                            <div class="ty-2-col-text col-md-5">
                                    {!! $contents[$i]->{'wysiwyg_' . app()->getLocale()} !!}
                            </div>
                        </div>
                    @endif
                @endif
            @endfor
        @endif
    </div>
</x-guest-layout>
