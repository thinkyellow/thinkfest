<x-guest-layout>
    <div class="container">
        <div class="row g-0">
            <div class="ty-public-title col-lg">
                <h1 class="ty-main-title ty-main-title-db ty-main-title-public">{{ __('FAQ') }}</h1>
            </div>
            <div class="ty-public-content col-lg">
                <div class="accordion" id="accordionFaq">


                    <h2>{{ __('Algemeen') }}</h2>

                    @foreach($faqs as $faq)
                    @if($faq->header == 'top-faq')
                    <div class="accordion-item">
                        <h3 class="accordion-header" id="heading{{ $faq->id }}">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $faq->id }}" aria-expanded="false" aria-controls="collapse{{ $faq->id }}">
                                {{ $faq->{'question_' . app()->getLocale()} }}
                            </button>
                        </h3>
                        <div id="collapse{{ $faq->id }}" class="accordion-collapse collapse" aria-labelledby="heading{{ $faq->id }}" data-bs-parent="#accordionFaq">
                            <div class="accordion-body">
                                {!! $faq->{'answer_' . app()->getLocale()} !!}
                            </div>
                        </div>
                    </div>
                    @endif()
                    @endforeach()

                    <h2>{{ __('Ik wil een activiteit organiseren') }}</h2>

                    @foreach($faqs as $faq)
                    @if($faq->header == 'middle-faq')
                    <div class="accordion-item">
                        <h3 class="accordion-header" id="heading{{ $faq->id }}">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $faq->id }}" aria-expanded="false" aria-controls="collapse{{ $faq->id }}">
                                {{ $faq->{'question_' . app()->getLocale()} }}
                            </button>
                        </h3>
                        <div id="collapse{{ $faq->id }}" class="accordion-collapse collapse" aria-labelledby="heading{{ $faq->id }}" data-bs-parent="#accordionFaq">
                            <div class="accordion-body">
                                {!! $faq->{'answer_' . app()->getLocale()} !!}
                            </div>
                        </div>
                    </div>
                    @endif()
                    @endforeach()

                    <h2>{{ __('Ik ben deelnemer') }}</h2>

                    @foreach($faqs as $faq)
                    @if($faq->header == 'bottom-faq')
                    <div class="accordion-item">
                        <h3 class="accordion-header" id="heading{{ $faq->id }}">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $faq->id }}" aria-expanded="false" aria-controls="collapse{{ $faq->id }}">
                                {{ $faq->{'question_' . app()->getLocale()} }}
                            </button>
                        </h3>
                        <div id="collapse{{ $faq->id }}" class="accordion-collapse collapse" aria-labelledby="heading{{ $faq->id }}" data-bs-parent="#accordionFaq">
                            <div class="accordion-body">
                                {!! $faq->{'answer_' . app()->getLocale()} !!}
                            </div>
                        </div>
                    </div>
                    @endif()
                    @endforeach()

                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
