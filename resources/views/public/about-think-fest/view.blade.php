<x-guest-layout>
    <div class="container">
        <div class="row g-0">
            <div class="ty-public-title col-lg">
                <h1 class="ty-main-title ty-main-title-db ty-main-title-public">{{ __('THiNK FeST: het verhaal') }}</h1>
            </div>
            <div class="ty-public-content col-lg">
                <div class="ty-text-block">
                    @foreach($contents as $content)
                        @if($content->position == 'header')
                            {!! $content->{'wysiwyg_'.app()->getLocale()} !!}
                            @if($content->{'button_'.app()->getLocale()} != null)
                                <a href="{{ $content->{'link_'.app()->getLocale()} }}" class="btn btn-radius-sm btn-green">{{ $content->{'button_'.app()->getLocale()} }}</a>
                            @endif()
                        @endif()
                    @endforeach()
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
