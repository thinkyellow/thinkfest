<div class="ty-schedule" id="programme">
    <div class="row align-items-center">
        <div class="col-lg-4">
            <h2>{{ __('Programma') }}</h2>
        </div>
        <div class="col-lg-4 offset-lg-4">
            <div class="ty-input-group input-group">
                <span class="input-group-text" id="basic-addon1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#6C757D" class="bi bi-search" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                    </svg>
                </span>
                <input id="myInput" placeholder="Zoeken" class="ty-input form-control" type="text">
            </div>
        </div>
        <div class="filter ty-checkbox col-12">
            <div class="btn-group" role="group" aria-label="Basic checkbox toggle button group">
                @foreach($tags as $tag)
                <input type="checkbox" class="filter-tag btn-check" id="{{ $tag->{'title_' . app()->getLocale()} }}" autocomplete="off">
                <label class="btn btn-radius-sm btn-outline-primary" for="{{ $tag->{'title_' . app()->getLocale()} }}">{{ $tag->{'title_' . app()->getLocale()} }}</label>
                @endforeach()
            </div>
        </div>
    </div>
    <div class="cd-schedule cd-schedule--loading margin-top-lg margin-bottom-lg js-cd-schedule">
        <div class="cd-schedule__timeline">
            <ul>
                <li><span>09:00</span></li>
                <li><span>09:30</span></li>
                <li><span>10:00</span></li>
                <li><span>10:30</span></li>
                <li><span>11:00</span></li>
                <li><span>11:30</span></li>
                <li><span>12:00</span></li>
                <li><span>12:30</span></li>
                <li><span>13:00</span></li>
                <li><span>13:30</span></li>
                <li><span>14:00</span></li>
                <li><span>14:30</span></li>
                <li><span>15:00</span></li>
                <li><span>15:30</span></li>
                <li><span>16:00</span></li>
                <li><span>16:30</span></li>
                <li><span>17:00</span></li>
            </ul>
        </div>

        <div class="cd-schedule__events">
            <ul id="program">

                @foreach($locations as $location)
                <li class="cd-schedule__group column">
                    <div class="cd-schedule__top-info"><span>{{ $location->room }}</span></div>
                    <ul>
                        <li class="cd-schedule__event ty-standard-event">
                            <a data-start="09:00" data-end="09:45" data-content="event-abs-circuit" data-event="event-1" href="#0">
                                <em class="cd-schedule__name">Inloop + Centrale aftrap</em>
                            </a>
                        </li>

                        @if($location->room == 'Atrium')
                            <li class="cd-schedule__event ty-standard-event">
                            <a data-start="09:50" data-end="11:55" data-content="event-abs-circuit" data-event="event-1" href="#0">
                                <em class="cd-schedule__name">Bezet</em>
                            </a>
                        </li>
                        @endif()

                        @foreach($location->activities as $activity)
                        <li class="cd-schedule__event activity">
                            <a data-start="{{ substr($activity->start_time, 0, -3) }}" data-end="{{ substr($activity->end_time, 0 , -3) }}" data-content="{{ $activity }}" data-event="event-2" href="#0">
                                <em class="cd-schedule__name">{{ $activity->title }}</em>
                                @foreach($activity->tags as $tag)
                                <span class="tags badge bg-{{ $tag->color }}">{{ $tag->{'title_' . app()->getLocale()} }}</span>
                                @endforeach()
                            </a>
                        </li>
                        @endforeach()

                        <li class="cd-schedule__event ty-standard-event">
                            <a data-start="12:00" data-end="13:00" data-content="event-abs-circuit" data-event="event-1" href="#0">
                                <em class="cd-schedule__name">Lunch</em>
                            </a>
                        </li>

                        @if($location->room == 'Atrium')
                            <li class="cd-schedule__event ty-standard-event">
                            <a data-start="13:05" data-end="15:55" data-content="event-abs-circuit" data-event="event-1" href="#0">
                                <em class="cd-schedule__name">Bezet</em>
                            </a>
                        </li>
                        @endif()

                        <li class="cd-schedule__event ty-standard-event">
                            <a data-start="16:00" data-end="17:00" data-content="event-abs-circuit" data-event="event-1" href="#0">
                                <em class="cd-schedule__name">Prijzenparade + Borrel</em>
                            </a>
                        </li>

                    </ul>
                </li>
                @endforeach()

            </ul>
        </div>

        <div class="ty-schedule-modal cd-schedule-modal">
            <header class="cd-schedule-modal__header">
                <div class="ty-schedule-modal-content cd-schedule-modal__content">
                    <span class="cd-schedule-modal__date"></span>
                    <h3 class="cd-schedule-modal__name"></h3>
                    @if(app()->getLocale() == 'nl')
                    <p><b>Organisator: </b><span class="ty-schedule-organiser"></span></p>
                    <p><b>Locatie: </b><span class="ty-schedule-location"></span></p>
                    <p><b>Deelnemers: </b> <span class="ty-schedule-part"></span></p>
                    <p><b>Taal: </b><span class="ty-schedule-language"></span></p>
                    Aanmelden is mogelijk vanaf 25 oktober.
                    @else
                    <p><b>Organizer: </b><span class="ty-schedule-organiser"></span></p>
                    <p><b>Location: </b><span class="ty-schedule-location"></span></p>
                    <p><b>Participants: </b> <span class="ty-schedule-part"></span></p>
                    <p><b>Language: </b><span class="ty-schedule-language"></span></p>
                    Signup will be possible from the 25th of October.
                    @endif

                    <form id="retire" method="POST">
                        @csrf
                        <button class="btn btn-radius-sm btn-danger">Afmelden</button>
                    </form>

                    <form id='participate' method="POST">
                        @csrf
                        <button class="btn btn-radius-sm btn-dark">Deelnemen</button>
                    </form>

                    <form id='favorite' method="POST">
                        @csrf
                        <button class="btn btn-radius-sm btn-dark">Favoriet</button>
                    </form>
                    
                    <form id='unfavorite' method="POST">
                        @csrf
                        <button class="btn btn-radius-sm btn-danger">Verwijder Favoriet</button>
                    </form>
                </div>
                <div class="cd-schedule-modal__header-bg"></div>
            </header>

            <div class="cd-schedule-modal__body">
                <div class="ty-event-info cd-schedule-modal__event-info">

                    <img class="ty-schedule-img" src="">
                    <div class="ty-schedule-text"></div>
                </div>
                <div class="cd-schedule-modal__body-bg"></div>
            </div>

            <a href="#0" class="cd-schedule-modal__close text-replace"></a>
        </div>

        <div class="cd-schedule__cover-layer"></div>
    </div>
</div>