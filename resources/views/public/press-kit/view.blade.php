<x-guest-layout>
    <div class="container">
        <div class="row g-0">
            <div class="ty-public-title col-lg">
                <h1 class="ty-main-title ty-main-title-db ty-main-title-public">{{ __('Toolkit') }}</h1>
            </div>
            <div class="ty-public-content col-lg">
                <div class="ty-text-block">
                    @foreach($contents as $content)
                        @if($content->position == 'header')
                            {!! $content->{'wysiwyg_'.app()->getLocale()} !!}
                            @if($content->{'button_'.app()->getLocale()} != null)
                                <a href="{{ $content->{'link_'.app()->getLocale()} }}" class="btn btn-radius-sm btn-green">{{ $content->{'button_'.app()->getLocale()} }}</a>
                            @endif()
                            <div class="download-wrapper">
                                <a class="logo-download" download="logo-thinkfest" href="{{ asset('images/logo.svg') }}" title="Logo THiNK-FeST">
                                    <img src="{{ asset('images/logo.svg') }}" alt="Logo THiNK-FeST"></img>
                                </a>
                                <a class="logo-download" download="logo-haagse-hogeschool" href="{{ asset('images/hhs_nl_groen_hex.png') }}" title="Logo Haagse Hogeschool">
                                    <img src="{{ asset('images/hhs_nl_groen_hex.png') }}" alt="Logo Haagse Hogeschool"></img>
                                </a>
                            </div>
                        @endif()
                    @endforeach()
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>

