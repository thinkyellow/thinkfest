<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <x-slot name="logo">
                <x-jet-authentication-card-logo />
            </x-slot>
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <x-jet-validation-errors class="mb-4" />

                    <form method="POST" action="{{ route('auth.register') }}">
                        @csrf

                        <div class="bg-white overflow-hidden rounded p-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-label" for="formName">{{ __('Name') }}</label>
                                    <input type="text" class="form-control" id="formName" name="name" placeholder="Name" value="{{ old('name') }}">
                                </div>

                                <div class="col-md-12 mt-4">
                                    <label class="form-label" for="formEmail">{{ __('Email') }}</label>
                                    <input type="text" class="form-control" id="formEmail" name="email" placeholder="Email" value="{{ old('email') }}">
                                </div>

                                <div class="col-md-12 mt-4">
                                    <label class="form-label" for="formPwd">{{ __('Password') }}</label>
                                    <input type="password" class="form-control" id="formPwd" name="password" value="{{ old('password') }}" required autocomplete="new-password">
                                </div>
                                <div class="flex items-center justify-end mt-4">
                                    <x-jet-button class="ml-4">
                                        {{ __('Register') }}
                                    </x-jet-button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
