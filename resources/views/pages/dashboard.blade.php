<x-app-layout>
    <div class="container">
        <div class="ty-dashboard row g-0">
            <div class="col-lg-5 col-xl-6">
                <h1 class="ty-main-title ty-main-title-db">Dashboard</h1>
                @can('create', \App\Models\Activity::class)
                <a class="btn btn-green" href="{{ route('pages.draft.create') }}">Create activity</a>
                @endcan()
                <div class="ty-dashboard-nav d-flex flex-column">
                    <a href="{{ route('pages.activity.list') }}">All activities</a>
                    {{-- <a href="{{ route('pages.activity.list') }}">Mijn activiteiten</a> --}}
                </div>
            </div>
            <div class="col-lg col-xl-6">
                <div class="ty-dashboard-stats d-flex flex-wrap">
                    @foreach($countArray as $key => $value)
                    <div class="ty-dashboard-stat d-flex justify-content-center align-items-center flex-column">
                        <p class="ty-dashboard-stat-number">{{ $value }}</p>
                        <p class="ty-dashboard-stat-cat">{{ __(ucwords($key)) }}</p>
                    </div>
                    @endforeach()
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
