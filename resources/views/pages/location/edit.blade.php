<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.location.list') }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Edit location</h1>
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <form action="{{ route('pages.location.update', ['location' => $location->id]) }}" method="POST" class="row g-4">
                        @csrf
                        @method('PUT')

                        <div class="bg-white overflow-hidden rounded p-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-bottom: 16px;">Fields with * are mandatory</p>
                                    <p><b>Warning</b>: To paste from word; use ctrl+shift+v for Windows and cmd+shift+v for Mac</p>
                                </div>
                                <div class="col-md-12">
                                    <label class="form-label" for="formGroupExampleInput">Room *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="room" placeholder="Room" value="{{ $location->room }}">
                                    @error('room')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Description *</label>
                                    <textarea name="location_description" placeholder="Description">{{ $location->location_description }}</textarea>
                                    @error('location_description')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Location details *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="location" placeholder="Location details" value="{{ $location->location }}">
                                    @error('location')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Image *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="image" placeholder="Image" value="{{ $location->image }}">
                                    @error('image')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Subtext *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="subtext" placeholder="Subtext" value="{{ $location->subtext }}">
                                    @error('subtext')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Maximum number of participants *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="max_possible_participants" placeholder="Maximum number of participants" value="{{ $location->max_possible_participants }}">
                                    @error('max_possible_participants')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-black ty-btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
