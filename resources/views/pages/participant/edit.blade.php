<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.participant.list', ['id' => $id]) }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Edit participants</h1>
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <form action="{{ route('pages.participant.add') }}" method="POST" class="row g-4">
                        @csrf

                        <div class="bg-white rounded p-4">
                            <div class="row">
                                <div class="col-md-4 mt-3">
                                    <label class="form-label" for="exampleFormControlSelect1">Add user:</label>
                                    <select name="users[]" class="form-control selectpicker" title="Select user" data-live-search="true" multiple>
                                        @foreach($users as $user)
                                        <option {{ $user->selected === true ? "selected" : "" }} value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach()
                                    </select>
                                    @error('users')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="visually-hidden">
                                    <input type="hidden" value="{{ $id }}" name="activityId">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-black ty-btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>