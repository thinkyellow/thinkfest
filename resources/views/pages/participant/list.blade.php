<x-app-layout>
    <div class="container">
        <div class="position-relative">
        <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.participant.edit', ['id' => $id]) }}" class="ty-list-btn btn btn-black">Add Participants</a>
            </div>
            <div class="ty-top-content row">
                <div class="col-lg-4">
                    <h1 class="ty-main-title">Participants</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="myTable" class="ty-table table table-light">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th onclick="sortTable(1)" scope="col">Name</th>
                            <th onclick="sortTable(2)" scope="col">Email</th>
                            <th scope="col">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($participants as $participant)
                        <tr class="align-middle">
                            <th scope="row">{{ $participant->id }}</th>
                            <td>{{ $participant->name }}</td>
                            <td>{{ $participant->email }}</td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <button title="Remove participant" type="button" class="ty-alert" data-toggle="modal" data-target="#confirmDelete{{ $participant->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                        </svg>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <div class="modal fade" id="confirmDelete{{ $participant->id }}" tabindex="-1" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="confirmDeleteLabel">Are you sure you wish to remove {{ $participant->name }}?</h5>
                                        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-black" data-dismiss="modal">Close</button>
                                        <form action="{{ route('pages.participant.retire', ['activityId' => $activity->id, 'participantId' => $participant->id]) }}" method="POST">
                                            @csrf
                                            <button class="btn btn-red">
                                                Remove
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach()
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
