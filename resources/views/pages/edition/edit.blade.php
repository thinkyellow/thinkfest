<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.edition.list') }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Edit edition</h1>
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <form action="{{ route('pages.edition.update', ['edition' => $edition->id]) }}" method="POST" class="row g-4">
                        @csrf
                        @method('PUT')

                        <div class="bg-white overflow-hidden rounded p-4">
                            <div class="form-check mt-3">
                                <input class="form-check-input" type="checkbox" name="is_active" value="1" {{ $edition->is_active ? 'checked disabled' : '' }} id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Active
                                </label>
                            </div>
                        </div>
                        <button class="btn btn-black ty-btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
