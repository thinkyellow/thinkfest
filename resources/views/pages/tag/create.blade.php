<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.tag.list') }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Create tag</h1>
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <form action="{{ route('pages.tag.store') }}" method="POST" class="row g-4">
                        @csrf
                        <div class="bg-white overflow-hidden rounded p-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-bottom: 16px;">Fields with * are mandatory</p>
                                    <p><b>Warning</b>: To paste from word; use ctrl+shift+v for Windows and cmd+shift+v for Mac</p>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput">Tag Dutch *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="title_nl" placeholder="Tag Dutch " value="{{ old('title_nl') }}">
                                    @error('title_nl')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput">Tag English</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="title_en" placeholder="Tag English" value="{{ old('title_en') }}">
                                    @error('title_en')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label class="form-label" for="exampleFormControlSelect1">Colour *</label>
                                    <select class="form-select" name="color" aria-label="Default select example">
                                        <option value="primary" @if( !old('color') ) selected @elseif(old('color')=='primary' ) selected @endif>Green</option>
                                        <option value="secondary" @if( old('color')=='secondary' ) selected @endif>Black</option>
                                        <option value="danger" @if( old('color')=='danger' ) selected @endif>Red</option>
                                        <option value="warning" @if( old('color')=='warning' ) selected @endif>Yellow</option>
                                        <option value="info" @if( old('color')=='info' ) selected @endif>Blue</option>
                                        <option value="light" @if( old('color')=='light' ) selected @endif>White</option>
                                    </select>
                                    @error('color')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-black ty-btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
