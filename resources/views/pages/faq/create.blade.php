<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.faq.list') }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Create FAQ</h1>
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <form action="{{ route('pages.faq.store') }}" method="POST" class="row g-4">
                        @csrf
                        <div class="bg-white overflow-hidden rounded p-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-bottom: 16px;">Fields with * are mandatory</p>
                                    <p><b>Warning</b>: To paste from word; use ctrl+shift+v for Windows and cmd+shift+v for Mac</p>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput">Question Dutch *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="question_nl" placeholder="Question Dutch" value="{{ old('question_nl') }}">
                                    @error('question_nl')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput">Question English</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="question_en" placeholder="Question English" value="{{ old('question_en') }}">
                                    @error('question_en')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Answer Dutch *</label>
                                    <textarea name="answer_nl" placeholder="Answer Dutch">{{ old('answer_nl') }}</textarea>
                                    @error('answer_nl')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Answer English</label>
                                    <textarea name="answer_en" placeholder="Answer English">{{ old('answer_en') }}</textarea>
                                    @error('answer_en')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label class="form-label" for="exampleFormControlSelect1">Below header: *</label>
                                    <select class="form-select" name="header" aria-label="Default select example">
                                        <option value="top-faq" @if( !old('header') ) selected @elseif(old('header')=='top-faq' ) selected @endif>General</option>
                                        <option value="middle-faq" @if( old('header')=='middle-faq' ) selected @endif>I want to organize an activity</option>
                                        <option value="bottom-faq" @if( old('header')=='bottom-faq' ) selected @endif>I am a participant</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-black ty-btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
