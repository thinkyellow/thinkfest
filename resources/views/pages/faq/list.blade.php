<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.faq.create') }}" class="ty-list-btn btn btn-black">Create FAQ</a>
            </div>
            <div class="ty-top-content row">
                <div class="col-lg-4">
                    <h1 class="ty-main-title">FAQ</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="myTable" class="ty-table table table-light">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th onclick="sortTable(1)" scope="col">Question</th>
                            <th onclick="sortTable(2)" scope="col">Answer</th>
                            <th scope="col">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($faqs as $faq)
                        <tr class="align-middle">
                            <th scope="row">{{ $faq->id }}</th>
                            <td>{{ (strlen($faq->question) > 25) ? substr($faq->question,0,25).'...' : $faq->question_nl; }}</td>
                            <td>{{ (strlen(strip_tags($faq->answer)) > 25) ? substr(strip_tags($faq->answer),0,25).'...' : strip_tags($faq->answer_nl); }}</td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <a href="{{ route('pages.faq.edit', ['id' => $faq->id]) }}" class="ty-primary mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                        </svg>
                                    </a>
                                    <button type="button" class="ty-alert" data-toggle="modal" data-target="#confirmDelete{{ $faq->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                        </svg>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <div class="modal fade" id="confirmDelete{{ $faq->id }}" tabindex="-1" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="confirmDeleteLabel">Are you sure you wish to remove {{ $faq->question }}?</h5>
                                        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-black" data-dismiss="modal">Close</button>
                                        <form action="{{ route('pages.faq.delete', ['faq' => $faq->id]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-red">
                                                Remove
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach()
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
