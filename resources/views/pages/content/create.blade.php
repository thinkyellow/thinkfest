<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.content.list') }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Create content</h1>
            <form action="{{ route('pages.content.store') }}" enctype="multipart/form-data" method="POST" class="row g-4">
                @csrf
                <div class="bg-white overflow-hidden rounded p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <p style="margin-bottom: 16px;">Fields with * are mandatory</p>
                            <p><b>Warning</b>: To paste from word; use ctrl+shift+v for Windows and cmd+shift+v for Mac</p>
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput2">Content Dutch *</label>
                            <textarea name="wysiwyg_nl" maxlength="500" placeholder="Content Dutch"></textarea>
                            @error('wysiwyg_nl')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput2">Content English</label>
                            <textarea name="wysiwyg_en" maxlength="500" placeholder="Content English"></textarea>
                            @error('wysiwyg_en')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput">Button Dutch</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="button_nl" placeholder="Button Dutch" value="{{ old('button_nl') }}">
                            @error('button_nl')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput">Button English</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="button_en" placeholder="Button English" value="{{ old('button_en') }}">
                            @error('button_en')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput">Link Dutch</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="link_nl" placeholder="Link Dutch" value="{{ old('link_nl') }}">
                            @error('link_nl')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput">Link English</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="link_en" placeholder="Link English" value="{{ old('link_en') }}">
                            @error('link_en')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-12 mt-3">
                            <label for="formFileSm" class="form-label">Select image</label>
                            <input class="form-control" name='image' id="formFileSm" type="file">
                            @error('image')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="exampleFormControlSelect1">Page *</label>
                            <select class="form-select" name="page" aria-label="Default select example">
                                <option value="Home" @if( old('page')=='Home' ) selected @endif>Home</option>
                                <option value="toolkit" @if( old('page')=='toolkit' ) selected @endif>Toolkit</option>
                                <option value="about-think-fest" @if( old('page')=='about-think-fest' ) selected @endif>THiNK FeST: het verhaal</option>
                                <option value="ednext" @if( old('page')=='ednext' ) selected @endif>EdNext</option>
                                <option value="privacy" @if( old('page')=='privacy' ) selected @endif>Privacy</option>
                                <option value="cookies" @if( old('page')=='cookies' ) selected @endif>Cookies</option>
                                <option value="contact" @if( old('page')=='contact' ) selected @endif>Contact</option>
                                <option value="draft" @if( old('page')=='draft' ) selected @endif>Concept</option>
                            </select>
                            @error('page')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="exampleFormControlSelect1">Position *</label>
                            <select class="form-select" name="position" aria-label="Default select example">
                                <option value="header" @if( old('position')=='header' ) selected @endif>Header</option>
                                <option value="bottom" @if( old('position')=='bottom' ) selected @endif>Bottom</option>
                                <option value="info" @if( old('position')=='info' ) selected @endif>Info</option>
                            </select>
                            @error('position')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <button class="btn btn-black ty-btn-save">Save</button>
            </form>
        </div>
    </div>
</x-app-layout>
