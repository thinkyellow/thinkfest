<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.content.list') }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Edit content</h1>
            <form action="{{ route('pages.content.update', ['content' => $content->id]) }}" enctype="multipart/form-data" method="POST" class="row g-4">
                @csrf
                @method('PUT')

                <div class="bg-white overflow-hidden rounded p-4">
                    <div class="row">
                        <div class="col-md-12 mt-3">
                            <p style="margin-bottom: 16px;">Fields with * are mandatory</p>
                            <p><b>Warning</b>: To paste from word; use ctrl+shift+v for Windows and cmd+shift+v for Mac</p>
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput2">Content Dutch *</label>
                            <textarea name="wysiwyg_nl" maxlength="500" placeholder="Content Dutch">{{ $content->wysiwyg_nl }}</textarea>
                            @error('wysiwyg_nl')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput2">Content English</label>
                            <textarea name="wysiwyg_en" maxlength="500" placeholder="Content English">{{ $content->wysiwyg_en }}</textarea>
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput">Button Dutch</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="button_nl" value="{{ $content->button_nl }}" placeholder="Button Dutch">
                            @error('button_nl')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput">Button English</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="button_en" placeholder="Button English" value="{{ $content->button_en }}">
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput">Link Dutch</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="link_nl" value="{{ $content->link_nl }}" placeholder="Link Dutch">
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="formGroupExampleInput">Link English</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="link_en" value="{{ $content->link_en }}" placeholder="Link English">
                        </div>
                        <div class="col-md-12 mt-3">
                            <label for="formFileSm" class="form-label">Add/change image</label>
                            <input class="form-control" name='image' id="formFileSm" type="file">
                            @error('image')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="exampleFormControlSelect1">Page</label>
                            <select class="form-select" name="page" aria-label="Default select example">
                                <option {{ $content->page == 'home' ? 'selected' : '' }} value="Home">Home</option>
                                <option {{ $content->page == 'toolkit' ? 'selected' : '' }} value="toolkit">Toolkit</option>
                                <option {{ $content->page == 'about-think-fest' ? 'selected' : '' }} value="about-think-fest">THiNK FeST: het verhaal</option>
                                <option value="ednext" {{ $content->page == 'ednext' ? 'selected' : '' }}>EdNext</option>
                                <option value="privacy" {{ $content->page == 'privacy' ? 'selected' : '' }}>Privacy</option>
                                <option value="cookies" {{ $content->page == 'cookies' ? 'selected' : '' }}>Cookies</option>
                                <option value="contact" {{ $content->page == 'contact' ? 'selected' : '' }}>Contact</option>
                                <option {{ $content->page == 'draft' ? 'selected' : '' }} value="draft">Concept</option>
                            </select>
                            @error('page')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mt-3">
                            <label class="form-label" for="exampleFormControlSelect1">Position</label>
                            <select class="form-select" name="position" aria-label="Default select example">
                                <option {{ $content->position == 'header' ? 'selected' : '' }} value="header">Header</option>
                                <option {{ $content->position == 'bottom' ? 'selected' : '' }} value="bottom">bottom</option>
                                <option {{ $content->position == 'info' ? 'selected' : '' }} value="info">Info</option>
                            </select>
                            @error('position')
                            <div class="ty-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <button class="btn btn-black ty-btn-save">Save</button>
            </form>
        </div>
    </div>
</x-app-layout>
