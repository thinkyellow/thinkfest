<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.draft.list') }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Edit concept</h1>
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <form action="{{ route('pages.draft.update', ['activity' => $activity->id]) }}" method="POST" enctype="multipart/form-data" class="row g-4">
                        @csrf
                        @method('PUT')
                        <div class="bg-white rounded p-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-bottom: 16px;">Fields with * are mandatory</p>
                                    <p><b>Warning</b>: To paste from word; use ctrl+shift+v for Windows and cmd+shift+v for Mac</p>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label class="form-label" for="formGroupExampleInput">Title *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput" name="title" placeholder="Title" value="{{ $activity->title }}">
                                    @error('title')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Description *</label>
                                    <textarea name="description" placeholder="Description">{{ $activity->description }}</textarea>
                                    @error('description')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                @can('updateExtra', $activity)
                                <div class="col-md-6 mt-3">
                                    <label id="range" for="customRange2" class="form-label">Max number of participants: {{ $activity->max_participants }}</label>
                                    <input id="test" value="{{ $activity->max_participants }}" name="max_participants" type="range" class="form-range" min="1" max="101" id="customRange2">
                                    @error('max_participants')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Language *</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="language" value="0" id="dutchRadio" @if( $activity->language == 0 ) checked @endif>
                                        <label class="form-label" class="form-check-label" for="dutchRadio">
                                            Dutch
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="language" value="1" id="engRadio" @if( $activity->language == 1 ) checked @endif>
                                        <label class="form-label" class="form-check-label" for="engRadio">
                                            English
                                        </label>
                                    </div>
                                    @error('language')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Preferred location *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="location_name" placeholder="Preferred location" value="{{ $activity->location_name }}">
                                    @error('location_name')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Assigned location *</label>
                                    <select name="location_id" class="form-control">
                                        <option value="">Select location</option>
                                        @foreach($locations as $location)
                                        <option value="{{ $location->id }}" @if( $activity->location_id == $location->id ) selected @endif>{{ $location->room }}</option>
                                        @endforeach()
                                    </select>
                                    @error('location_id')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Change start time *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="start_time" placeholder="HH:MM" value="{{ $activity->start_time }}">
                                    @error('start_time')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Change end time *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="end_time" placeholder="HH:MM" value="{{ $activity->end_time }}">
                                    @error('end_time')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3 file-upload">
                                    <span class="image-path">{{ $activity->image_path }}</span>
                                    <label for="formFileSm" class="form-label">Add/change image *</label>
                                    <input class="form-control" name='image' id="formFileSm" type="file">
                                    @error('image')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label">Current image</label>
                                    <img class="img-thumbnail w-50" src="{{ asset('images/activities/' . $activity->image_path) }}" alt="" />
                                </div>
                                <div class="col-md-4 mt-3">
                                    <label class="form-label" for="exampleFormControlSelect1">Add organizers:</label>
                                    <select name="organizer_id[]" class="form-control selectpicker" title="Select organizer" data-live-search="true" multiple>
                                        @foreach($organizers as $organizer)
                                            <option {{ $organizer->selected === true ? "selected" : "" }} value="{{ $organizer->id }}">{{ $organizer->name }}</option>
                                        @endforeach()
                                    </select>
                                    @error('organizer_id')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-4 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Original organizer *</label>
                                    <select name="user_id" class="form-control">
                                        <option value="{{ $activity->user_id }}">{{ $name }}</option>
                                        @foreach($organizers as $organizer)
                                        <option value="{{ $organizer->id }}">{{ $organizer->name }}</option>
                                        @endforeach()
                                    </select>
                                    @error('user_id')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-md-4 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2" multiple>Tags</label>
                                    <select name="tag[]" class="form-control selectpicker" title="Select tags" data-live-search="true" multiple>
                                        <option value="">Select a tag</option>
                                        @foreach($tags as $tag)
                                        <option {{ $tag->selected === true ? "selected" : "" }} value="{{ $tag->id }}">{{ $tag->title_nl }}</option>
                                        @endforeach()
                                    </select>
                                    @error('tags')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-md-6 mt-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="is_published" value="1" {{ $activity->is_published ? 'checked' : '' }} id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Make public
                                        </label>
                                    </div>
                                </div>
                                <input type="hidden" id="removedTags" name="removedTags" value="">
                                <div class="col-md-6 mt-3">
                                    @foreach($activity->tags as $tag)
                                    <span id="{{ $tag->id }}" class="tag-list badge bg-{{ $tag->color }}">{{ $tag->title }}</span>
                                    @endforeach()
                                </div>
                                @endcan()
                            </div>
                        </div>
                        <button class="btn btn-black ty-btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script type="text/javascript">
var userLang = navigator.language || navigator.userLanguage;
if(userLang === 'en-US' || userLang === 'en-GB') {
    document.querySelector('.image-path').classList.add('path-en');
} else if (userLang === 'nl-NL') {
    document.querySelector('.image-path').classList.add('path-nl');
}
</script>
