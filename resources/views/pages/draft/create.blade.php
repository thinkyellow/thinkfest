<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.draft.list') }}" class="ty-list-btn btn btn-black">Back</a>
            </div>
            <h1 class="ty-main-title">Create activity</h1>
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <form action="{{ route('pages.draft.store') }}" method="POST" enctype="multipart/form-data" class="row g-4">
                        @csrf
                        <div class="bg-white rounded p-4">
                            <div class="ty-text">
                                @if($content != null)
                                {!! $content->wysiwyg_en !!}
                                @endif()
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-bottom: 16px;">Fields with * are mandatory</p>
                                    <p><b>Warning</b>: To paste from word; use ctrl+shift+v for Windows and cmd+shift+v for Mac</p>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label class="form-label" for="formGroupExampleInput">Title *</label>
                                    <input type="text" class="form-control" maxlength="75" id="formGroupExampleInput" name="title" placeholder="Title" value="{{ old('title') }}">
                                    @error('title')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror()
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Description *</label>
                                    <textarea name="description" maxlength="500" placeholder="Description"></textarea>
                                    @error('description')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label id="range" for="customRange2" class="form-label">Max number of participants: @if( old('max_participants') ) {{ old('max_participants') }} @else 50 @endif</label>
                                    <input id="test" name="max_participants" type="range" class="form-range" min="1" max="101" id="customRange2" value="{{ old('max_participants') }}">
                                    @error('max_participants')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-3 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Language *</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="language" value="0" id="dutchRadio" @if( old('language') ) @if( old('language')==0 ) checked @endif @else checked @endif>
                                        <label class="form-label" class="form-check-label" for="dutchRadio">
                                            Dutch
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="language" value="1" id="engRadio" @if( old('language')==1 ) checked @endif>
                                        <label class="form-label" class="form-check-label" for="engRadio">
                                            English
                                        </label>
                                    </div>
                                    @error('language')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="formGroupExampleInput2">Preferred Location *</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="location_name" placeholder="Location" value="{{ old('location_name') }}">
                                    @error('location_name')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="exampleFormControlSelect1">Select time *</label>
                                    <select class="form-select" name="activity_time" aria-label="Default select example">
                                        <option value="" @if( !old('end_time') ) selected @elseif(old('color')=='primary' ) selected @endif>Select time</option>
                                        <option value="10:00|10:45" @if( old('end_time')=='10:00|10:45' ) selected @endif>10:00 - 10:45 (45 min.)</option>
                                        <option value="10:00|11:45" @if( old('end_time')=='10:00|11:45' ) selected @endif>10:00 - 11:45 (105 min.)</option>
                                        <option value="11:00|11:45" @if( old('end_time')=='11:00|11:45' ) selected @endif>11:00 - 11:45 (45 min.)</option>
                                        <option value="13:00|13:45" @if( old('end_time')=='13:00|13:45' ) selected @endif>13:00 - 13:45 (45 min.)</option>
                                        <option value="13:00|14:45" @if( old('end_time')=='13:00|14:45' ) selected @endif>13:00 - 14:45 (105 min.)</option>
                                        <option value="13:00|15:45" @if( old('end_time')=='13:00|15:45' ) selected @endif>13:00 - 15:45 (165 min.)</option>
                                        <option value="14:00|14:45" @if( old('end_time')=='14:00|14:45' ) selected @endif>14:00 - 14:45 (45 min.)</option>
                                        <option value="14:00|15:45" @if( old('end_time')=='14:00|15:45' ) selected @endif>14:00 - 15:45 (105 min.)</option>
                                        <option value="15:00|15:45" @if( old('end_time')=='15:00|15:45' ) selected @endif>15:00 - 15:45 (45 min.)</option>
                                    </select>
                                    @error('activity_time')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label for="formFileSm" class="form-label">Select image *</label>
                                    <input class="form-control" name='image' id="formFileSm" type="file">
                                    @error('image')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label class="form-label" for="exampleFormControlSelect1">Add more organizers:</label>
                                    <select name="organizer_id" class="form-control selectpicker" id="exampleFormControlSelect1" title="Select organizer" data-live-search="true" multiple>
                                        @foreach($organizers as $organizer)
                                        <option value="{{ $organizer->id }}" @if( old('organizer_id')==$organizer->id ) selected @endif>{{ $organizer->name }}</option>
                                        @endforeach()
                                    </select>
                                    @error('organizer_id')
                                    <div class="ty-danger">{{ $message }}</div>
                                    @enderror
                                    <strong><i>Note: as organizer, you are automatically added</i></strong>
                                </div>

                                <div class="visually-hidden">
                                    <input type="hidden" value="{{ auth()->id() }}" id="formGroupExampleInput2" name="user_id">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-black ty-btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
