<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="ty-top-content row">
                <div class="col-lg-4">
                    <h1 class="ty-main-title">Users</h1>
                </div>
            </div>

            <div class="table-responsive">
                <table id="myTable" class="ty-table table table-light">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th onclick="sortTable(1)" scope="col">Name</th>
                            <th onclick="sortTable(2)" scope="col">Email</th>
                            <th onclick="sortTable(8)" scope="col">Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $activity)
                        <tr class="align-middle">
                            <td>{{ $activity->id }}</td>
                            <td>{{ $activity->name }}</td>
                            <td>{{ $activity->email }}</td>
                            <td>{{ $activity->created_at }}</td>
                        </tr>
                        @endforeach()
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
