<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="ty-top-content row">
                <div class="col-lg-4">
                    <h1 class="ty-main-title">Recycle bin</h1>
                </div>
            </div>

            <div class="table-responsive">
                <table id="myTable" class="ty-table table table-light">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th onclick="sortTable(1)" scope="col">Title</th>
                            <th onclick="sortTable(2)" scope="col">Description</th>
                            <th onclick="sortTable(8)" scope="col">Image</th>
                            <th onclick="sortTable(3)" scope="col">Start time</th>
                            <th onclick="sortTable(4)" scope="col">End time</th>
                            <th onclick="sortTable(5)" scope="col">Date</th>
                            <th onclick="sortTable(7)" scope="col">Participants</th>
                            <th onclick="sortTable(8)" scope="col">Language</th>
                            <th onclick="sortTable(8)" scope="col">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($trashed as $activity)
                        <tr class="align-middle">
                            <th scope="row">{{ $activity->id }}</th>
                            <td data-toggle="tooltip" data-original-title="{{ $activity->title }}">{{ (strlen($activity->title) > 25) ? substr($activity->title,0,25).'...' : $activity->title; }}</td>
                            <td data-toggle="tooltip" data-original-title="{{ $activity->description }}">{!! (strlen($activity->description) > 25) ? substr($activity->description,0,25).'...' : $activity->description; !!}</td>
                            @if ($activity->image_path == "" || $activity->image_path == null)
                                <td>&nbsp;</td>
                            @else
                                <td><img class="img-thumbnail w-20" src="{{ asset('images/activities/' . $activity->image_path) }}" alt="" /></td>
                            @endif
                            <td>{{ date('G:i', strtotime($activity->start_time)) }}</td>
                            <td>{{ date('G:i', strtotime($activity->end_time)) }}</td>
                            <td>{{ $activity->date }}</td>
                            <td>{{ count($activity->participants) . '/' . $activity->max_participants }}</td>
                            <td>{{ $activity->language ? 'Nederlands' : 'Engels' }}</td>
                            <td>
                                <div class="d-flex align-items-center">
                                    @can('update', $activity)
                                    <form action="{{ route('pages.trash.restore', ['id' => $activity->id]) }}" method="POST" class="d-flex">
                                        @csrf
                                        @method('PUT')
                                        <button class="ty-primary mr-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-counterclockwise" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z"/>
                                                <path d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z"/>
                                            </svg>
                                        </button>
                                    </form>
                                    @endcan()
                                    @can('delete', $activity)
                                    <button type="button" class="ty-alert" data-toggle="modal" data-target="#confirmDelete{{ $activity->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                        </svg>
                                    </button>
                                    @endcan()
                                </div>
                            </td>
                        </tr>
                        <div class="modal fade" id="confirmDelete{{ $activity->id }}" tabindex="-1" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="confirmDeleteLabel">Are you sure you wish to permantently remove {{ $activity->title }}?</h5>
                                        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-black" data-dismiss="modal">Close</button>
                                        <form action="{{ route('pages.trash.delete', ['id' => $activity->id]) }}" method="POST" class="d-flex">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-red">
                                                Remove
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach()
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
