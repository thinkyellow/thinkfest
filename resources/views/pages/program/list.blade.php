<x-app-layout>
    <div class="container">
        <div class="position-relative">
            <div class="text-end position-absolute right-0">
                <a href="{{ route('pages.program.favorite') }}" class="ty-list-btn btn btn-black">My Favourites</a>
            </div>
            <div class="ty-top-content row">
                <div class="col-lg-4">
                    <h1 class="ty-main-title">My Programme</h1>
                </div>
            </div>

            <div class="table-responsive">
                <table id="myTable" class="ty-table table table-light">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th onclick="sortTable(1)" scope="col">Organizer(s)</th>
                            <th onclick="sortTable(2)" scope="col">Title</th>
                            <th onclick="sortTable(3)" scope="col">Description</th>
                            <th onclick="sortTable(4)" scope="col">Image</th>
                            <th onclick="sortTable(5)" scope="col">Start time</th>
                            <th onclick="sortTable(6)" scope="col">End time</th>
                            <th onclick="sortTable(7)" scope="col">Participants</th>
                            <th onclick="sortTable(8)" scope="col">Language</th>
                            <th onclick="sortTable(9)" scope="col">Preferred location</th>
                            <th onclick="sortTable(10)" scope="col">Definitive location</th>
                            <th onclick="sortTable(11)" scope="col">Tags</th>
                            <th onclick="sortTable(11)" scope="col">Options</th>
                            @can('cre ate', \App\Models\Activity::class)
                            <th scope="col">Options</th>
                            @endcan
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($activities as $activity)
                        @can('view', $activity)
                        <tr class="align-middle">
                            <th scope="row">{{ $activity->id }}</th>
                            <th>
                                @foreach($activity->organizers as $organizer)
                                {{ $organizer->name }},
                                @endforeach()
                            </th>
                            <td>{{ substr_replace($activity->title, "...", 100) }}</td>
                            <td>{{ substr_replace(strip_tags($activity->description), "...", 50) }}</td>
                            @if ($activity->image_path == "" || $activity->image_path == null)
                            <td>&nbsp;</td>
                            @else
                            <td><img class="img-thumbnail w-20" src="{{ asset('images/activities/' . $activity->image_path) }}" alt="" /></td>
                            @endif
                            <td>{{ date('G:i', strtotime($activity->start_time)) }}</td>
                            <td>{{ date('G:i', strtotime($activity->end_time)) }}</td>
                            <td>{{ count($activity->participants) . '/' . $activity->max_participants }}</td>
                            <td>{{ $activity->language == 0 ? 'Nederlands' : 'Engels' }}</td>
                            <td>{{ $activity->location_name }}</td>
                            @if ($activity->location != null)
                            <td>{{ $activity->location->room }}</td>
                            @else
                            <td> </td>
                            @endif()
                            <td>
                                @foreach($activity->tags as $tag)
                                <span class="badge bg-{{ $tag->color }}">{{ $tag->title }}</span>
                                @endforeach()
                            </td>
                            @can('update', $activity)
                            <td>
                                <div class="d-flex align-items-center">
                                    @can('delete', $activity)
                                    <button title="Remove from my programme" type="button" class="ty-alert mr-3" data-toggle="modal" data-target="#confirmDelete{{ $activity->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                        </svg>
                                    </button>
                                    @endcan()
                                    
                                    @can('update', $activity)
                                    <a title="Edit Participants" class="ty-primary mr-3" href="{{ route('pages.participant.list', ['id' => $activity->id]) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-lines-fill" viewBox="0 0 16 16">
                                            <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z" />
                                        </svg>
                                    </a>
                                    @endcan()
                                </div>
                            </td>
                            @endcan()
                        </tr>
                        <div class="modal fade" id="confirmDelete{{ $activity->id }}" tabindex="-1" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="confirmDeleteLabel">Are you sure you wish to remove {{ $activity->title }}?</h5>
                                        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-black" data-dismiss="modal">Close</button>
                                        <form action="{{ route('pages.activity.delete', ['activity' => $activity->id]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-red">
                                                Remove
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endcan()
                        @endforeach()
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>