<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <script>document.getElementsByTagName("html")[0].className += " js";</script>

    <!-- Favicons -->
    <link rel="icon" type="image/png" sizes="36x36" href="{{ url('images/favicons/android-chrome-36x36.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="36x36" href="{{ url('images/favicons/android-chrome-36x36.png') }}">
    <link rel="icon" type="image/png" sizes="48x48" href="{{ url('images/favicons/android-chrome-48x48.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="48x48" href="{{ url('images/favicons/android-chrome-48x48.png') }}">
    <link rel="icon" type="image/png" sizes="72x72" href="{{ url('images/favicons/android-chrome-72x72.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="{{ url('images/favicons/android-chrome-72x72.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('images/favicons/android-chrome-96x96.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="96x96" href="{{ url('images/favicons/android-chrome-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="144x144" href="{{ url('images/favicons/android-chrome-144x144.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{ url('images/favicons/android-chrome-144x144.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('images/favicons/android-chrome-192x192.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="192x192" href="{{ url('images/favicons/android-chrome-192x192.png') }}">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="https://use.typekit.net/arc7dxq.css">

    <!-- wysiwyg -->
    <script src="https://cdn.tiny.cloud/1/5to7gm5fdnl8h5vy41vt9ddo0p11nwm5w25l8swwdn72exa6/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">

    <link rel="stylesheet" href="{{ url('css/schedule/style.css') }}">

    @livewireStyles

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>

<body class="antialiased">
    <div class="min-h-screen ty-bg-guest {{ request()->segment(count(request()->segments())) }}">
        <div class="ty-logo-guest-hh">
            <div class="container">
                <img src="{{ url('images/logo_hh.svg') }}">
            </div>
        </div>

        @include('navigation-menu-public')

        @if(session()->has('message'))
        <div class="container">
            <div class="alert alert-success position-relative">
                {{ session()->get('message') }}
            </div>
        </div>
        @endif

        @if(session()->has('error'))
        <div class="container">
            <div class="alert alert-alert">
                {{ session()->get('error') }}
            </div>
        </div>
        @endif

        <!-- Page Content -->
        <main>
            {{ $slot }}
        </main>
    </div>

    @include('footer-public')

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ url('js/schedule/util.js') }}" defer></script>
    <script src="{{ url('js/schedule/main.js') }}" defer></script>
    <script src="{{ url('js/script.js') }}" defer></script>
    @auth
    <script>
        const USER_ID = "{{ auth()->user()->id }}"
    </script>
    @endauth
</body>

</html>
