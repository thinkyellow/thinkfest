<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Favicons -->
    <link rel="icon" type="image/png" sizes="36x36" href="{{ url('images/favicons/android-chrome-36x36.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="36x36" href="{{ url('images/favicons/android-chrome-36x36.png') }}">
    <link rel="icon" type="image/png" sizes="48x48" href="{{ url('images/favicons/android-chrome-48x48.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="48x48" href="{{ url('images/favicons/android-chrome-48x48.png') }}">
    <link rel="icon" type="image/png" sizes="72x72" href="{{ url('images/favicons/android-chrome-72x72.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="{{ url('images/favicons/android-chrome-72x72.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('images/favicons/android-chrome-96x96.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="96x96" href="{{ url('images/favicons/android-chrome-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="144x144" href="{{ url('images/favicons/android-chrome-144x144.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{ url('images/favicons/android-chrome-144x144.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('images/favicons/android-chrome-192x192.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="192x192" href="{{ url('images/favicons/android-chrome-192x192.png') }}">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="https://use.typekit.net/arc7dxq.css">

    <!-- wysiwyg -->
    <script src="https://cdn.tiny.cloud/1/5to7gm5fdnl8h5vy41vt9ddo0p11nwm5w25l8swwdn72exa6/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    @livewireStyles

    <!-- Scripts -->
</head>

<body class="antialiased">
    <x-jet-banner />

    <div class="min-h-screen ty-bg-hh">
        @livewire('navigation-menu')

        <!-- Page Heading -->
        @if (isset($header))
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                {{ $header }}
            </div>
        </header>
        @endif

        @if(session()->has('message'))
        <div class="container">
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        </div>
        @endif

        <!-- Page Content -->
        <main>
            {{ $slot }}
        </main>
    </div>

    @stack('modals')

    @include('footer')

    @livewireScripts
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="{{ url('js/script.js') }}" defer></script>
    <script>
        tinymce.init({
            menubar: false,
            max_chars: "500",
            selector: 'textarea',
            inline_styles: true,
            eep_styles: true,
            relative_urls: false,
            remove_script_host: false,
            document_base_url: 'https://thinkfest.nl/',
            plugins: 'link lists advlist',
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | link bullist numlist'
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.js"></script>
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
</body>

</html>