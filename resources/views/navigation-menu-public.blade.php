<nav class="ty-nav">
    <div class="container">
        @guest
        <img class="ty-guest-logo" src="{{ url('images/logo_public.svg') }}">
        @endguest
        @auth
        <div class="navbar navbar-light navbar-expand-lg bg-transparent">
            <div class="ty-nav-container bg-white rounded">
                <a href="{{ route('public.dashboard', app()->getLocale()) }}"><img src="{{ url('images/logo.svg') }}"></a>
                @auth
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarItems" aria-controls="navbarItems" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarItems">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('public.dashboard')) active @endif" aria-current="page" href="{{ route('public.dashboard', app()->getLocale()) }}">{{ __('Home') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('public.dashboard')) active @endif" aria-current="page" href="{{ route('public.dashboard', app()->getLocale()) }}#programme">{{ __('Programma') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('public.press-kit')) active @endif" aria-current="page" href="{{ route('public.press-kit', app()->getLocale()) }}">{{ __('Toolkit') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('public.about-think-fest')) active @endif" aria-current="page" href="{{ route('public.about-think-fest', app()->getLocale()) }}">{{ __('THiNK FeST: het verhaal') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('public.ednext')) active @endif" aria-current="page" href="{{ route('public.ednext', app()->getLocale()) }}">EdNext</a>
                        </li>
                        <li class="nav-item">
                            <a class="ty-nav-link nav-link @if(request()->routeIs('public.faq')) active @endif" aria-current="page" href="{{ route('public.faq', app()->getLocale()) }}">{{ __('FAQ') }}</a>
                        </li>
                    </ul>
                    <div class="ty-dropdown dropdown d-none d-lg-flex">
                        @switch(app()->getLocale())
                            @case('en')
                                <a class="ty-nav-link nav-link" href="{{ route('public.dashboard', 'nl') }}">Nederlands</a>
                                @break
                            @case('nl')
                                <a class="ty-nav-link nav-link" href="{{ route('public.dashboard', 'en') }}">English</a>
                                @break
                        @endswitch
                    </div>
                    <div class="d-flex ty-nav-profile-box">
                        <li class="nav-item dropdown">
                            <a class="ty-nav-profile d-flex align-items-center rounded border nav-link" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <p>{{ auth()->user()->name }}</p>
                            </a>
                            <ul class="dropdown-menu rounded" aria-labelledby="navbarScrollingDropdown">
                                @if (Auth::user()->role_id == 30)
                                    <li><a class="dropdown-item" href="{{ route('dashboard') }}">{{ __('Beheren') }}</a></li>
                                @else()
                                    @if('2021-10-23 00:00:00' >= \Carbon\Carbon::now())
                                        <li><a class="dropdown-item" href="{{ route('dashboard') }}">{{ __('Beheren') }}</a></li>
                                    @endif()
                                @endif()
                                
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">{{ __('Uitloggen') }}</a>
                                    </li>
                                </form>
                            </ul>
                        </li>
                    </div>
                </div>
                @endauth
            </div>
        </div>
        @endauth
    </div>
</nav>
