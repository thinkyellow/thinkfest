@component('mail::message')
<img src="{{ url('https://thinkfest.nl/images/logo.svg') }}">
<br><br>

Beste {{ Auth::user()->name }},

Sad to see you go! Je bent nu uitgeschreven voor ‘{{ $activity->title }}’ om {{ $activity->start_time }} in {{ $location->room }} is gelukt. Je kan je persoonlijke festivalprogramma bekijken via <a style="color:#CA433C" href="https://thinkfest.nl/admin/program" target="_blank">deze link</a>.

Heb je een vraag over THiNK FeST? Kijk even in de FAQ <a style="color:#CA433C" href="https://www.thinkfest.nl/nl/public/faq" target="_blank">FAQ</a>. Staat je vraag er niet tussen? Neem dan contact ons op via de mail <a style="color:#CA433C" href="mailto:thinkfest@hhs.nl" target="_blank">thinkfest@hhs.nl</a>.

Met vriendelijke groet,<br>
Team {{ config('app.name') }}

<hr><br>

Dear {{ Auth::user()->name }},<br>

Sad to see you go! You are no longer registered for ‘{{ $activity->title }}’ at {{ $activity->start_time }} in {{ $location->room }}. You can view your personal festival program via <a style="color:#CA433C" href="https://thinkfest.nl/admin/program" target="_blank">this link</a>.

Do you have a question about THiNK FeST? Please take a look at the FAQ <a style="color:#CA433C" href="https://www.thinkfest.nl/en/public/faq" target="_blank">FAQ</a>. Is your question not answered? Please contact us by e-mail <a style="color:#CA433C" href="mailto:thinkfest@hhs.nl" target="_blank">thinkfest@hhs.nl</a>.

Kind regards,<br>
Team {{ config('app.name') }}

@endcomponent()
