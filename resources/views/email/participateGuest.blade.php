@component('mail::message')
<img src="{{ url('https://thinkfest.nl/images/logo.svg') }}">
<br><br>

Beste {{ Auth::user()->name }},

Yes! Je inschrijving voor ‘{{ $activity->title }}’ om {{ $activity->start_time }} in {{ $location->room }} is gelukt. Je kan je persoonlijke festivalprogramma bekijken via <a style="color:#CA433C" href="https://www.thinkfest.nl/admin/activity/list" target="_blank">deze link</a>.

Mocht je vragen hebben over de activiteit, kun je contact opnemen met de organisator: {{ $organizer->name }}.

Heb je een vraag over THiNK FeST? Kijk even in de FAQ <a style="color:#CA433C" href="https://www.thinkfest.nl/nl/public/faq" target="_blank">FAQ</a>. Staat je vraag er niet tussen? Neem dan contact ons op via de mail <a style="color:#CA433C" href="mailto:thinkfest@hhs.nl" target="_blank">thinkfest@hhs.nl</a>.

Veel plezier!

Met vriendelijke groet,<br>
Team {{ config('app.name') }}

<hr><br>

Dear {{ Auth::user()->name }},<br>

Yes! Your registration for ‘{{ $activity->title }}’ at {{ $activity->start_time }} in {{ $location->room }} has been successful. You can view your personal festival program via <a style="color:#CA433C" href="https://www.thinkfest.nl/admin/activity/list" target="_blank">this link</a>.

If you have any questions about the activity, please contact the organizer: {{ $organizer->name }}.

Do you have a question about THiNK FeST? Please take a look at the <a style="color:#CA433C" href="https://www.thinkfest.nl/en/public/faq" target="_blank">FAQ</a>. Is your question not answered? Please contact us by e-mail <a style="color:#CA433C" href="mailto:thinkfest@hhs.nl" target="_blank">thinkfest@hhs.nl</a>.

Have a good time!

Kind regards,<br>
Team {{ config('app.name') }}
@endcomponent()




