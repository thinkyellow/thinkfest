@component('mail::message')
<img src="{{ url('https://thinkfest.nl/images/logo.svg') }}">
<br><br>

Beste {{ Auth::user()->name }},<br>

Wat leuk dat je een activiteit wil organiseren tijdens THiNK FeST op donderdag 4 november 2021! We hebben je aanvraag in goede orde ontvangen, lopen alles na en zullen jouw activiteit zo snel mogelijk publiceren op <a style="color:#CA433C" href="https://www.thinkfest.nl" target="_blank">www.thinkfest.nl</a>.<br>

Heb je een vraag? <a style="color:#CA433C" href="https://www.thinkfest.nl/nl/public/faq" target="_blank">Kijk even in de FAQ</a>. Staat je vraag er niet tussen? Neem dan contact ons op via de mail <a style="color:#CA433C" href="mailto:thinkfest@hhs.nl" target="_blank">thinkfest@hhs.nl</a>.<br>

Met vriendelijke groet,<br>
Team {{ config('app.name') }}

<hr>
<br>

<img src="{{ url('https://thinkfest.nl/images/logo.svg') }}">
<br><br>

Dear {{ Auth::user()->name }},

How nice that you are going to organize an activity during THiNK FeST on Thursday November 4 2021! We have received your application in good order, we will check everything and will publish your activity on www.thinkfest.nl as soon as possible.

Do you have a question? <a style="color:#CA433C" href="https://www.thinkfest.nl/en/public/faq" target="_blank">Please take a look in the FAQ</a>. Is your question not answered? Please contact us by e-mail <a style="color:#CA433C" href="mailto:thinkfest@hhs.nl" target="_blank">thinkfest@hhs.nl</a>.

Kind regards,<br>
Team {{ config('app.name') }}
@endcomponent
