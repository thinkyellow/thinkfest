@component('mail::message')
# Introduction

Beste {{ $activity->organizers[0]->name }},

Gefeliciteerd! Jouw activiteit '{{ $activity->title }}' is goedgekeurd. Vanaf nu is je activiteit zichtbaar in het programma op www.thinkfest.nl. Vanaf maandag 25 oktober 2021 kunnen collega’s en studenten zich aanmelden voor jouw activiteit.

Wil je toch iets wijzigen (bijv. de titel of beschrijving)? Dat kan via de website bij <a style="color:#CA433C" href="https://thinkfest.nl/admin/activity/list" target="_blank">Mijn Activiteiten</a> tot en met vrijdag 22 oktober 2021.

Voorbereiding van jouw activiteit
<ul>
<li>Maak gebruik van de normale route (faculteits- of dienstbureau voor standaardbenodigdheden als post-its, stiften, flip-overs etc. en <a style="color:#CA433C" href="https://ifrontoffice.hhs.nl" target="_blank">iFrontOffice</a> voor IT-voorzieningen.)</li>
<li>De ruimtes zijn ingericht met standaard faciliteiten. Weet je niet zeker welke faciliteiten dat zijn? Loop dan in de komende dagen even langs jouw THiNK FeST locatie, zodat je op de dag zelf niet voor verrassingen komt te staan.</li>
<li>Tijdens THiNK FeST zorg je zelf voor de passende inrichting van je ruimte en zorg je dat de ruimte er na afloop weer uitziet, zoals je hem hebt aangetroffen. Daar hebben we een kwartier voor en na de activiteit voor ingepland.</li>
</ul>

Heb je een vraag? <a style="color:#CA433C" href="https://www.thinkfest.nl/nl/public/faq" target="_blank">Kijk even in de FAQ</a>. Staat je vraag er niet tussen? Neem dan contact ons op via de mail <a style="color:#CA433C" href="mailto:thinkfest@hhs.nl" target="_blank">thinkfest@hhs.nl</a>.

Met vriendelijke groet,<br>
Team {{ config('app.name') }}

<hr><br>

Dear {{ $activity->organizers[0]->name }},

Congratulations! Your activity '{{ $activity->title }}' has been approved. From now on, your activity is visible in the program at www.thinkfest.nl. From Monday 25 October 2021, colleagues and students can register for your activity.

If necessary you can edit your activity (e.g. the title or description) on the website at <a style="color:#CA433C" href="https://thinkfest.nl/admin/activity/list" target="_blank">My Activities</a>. This is possible until Friday 22 October 2021.

Prepare your activity
<ul>
<li>Use the normal route (faculty or service office for standard supplies such as post-its, markers, flip charts, etc. and <a style="color:#CA433C" href="https://ifrontoffice.hhs.nl" target="_blank">iFrontOffice</a> for IT facilities.)</li>
<li>The rooms are equipped with standard facilities. Are you not sure which facilities are available? Go and check your THiNK FeST location in the coming days, so that you will not face any surprises on the day itself.</li>
<li>During THiNK FeST, you are responsible for an appropriate arrangement of your room. Make sure the room is tidy when you leave. We have scheduled 15 minutes before and after the activity.</li>
</ul>

Do you have a question? <a style="color:#CA433C" href="https://www.thinkfest.nl/en/public/faq" target="_blank">Please take a look at the FAQ</a>. Is your question not answered? Please contact us by e-mail <a style="color:#CA433C" href="mailto:thinkfest@hhs.nl" target="_blank">thinkfest@hhs.nl</a>.

Kind regards,<br>
Team {{ config('app.name') }}


@endcomponent
