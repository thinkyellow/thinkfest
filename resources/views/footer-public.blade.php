
<footer class="ty-footer-public">
    <div class="container">
        <div class="ty-footer-public-content">
            <p>&copy; {{ __('THiNK FeST is een onderdeel van De Haagse Hogeschool') }} {{ now()->year }}. {{ __('Alle rechten voorbehouden.') }}</p>
            @auth
            <ul>
                <li><a href="{{ route('public.contact', app()->getLocale()) }}">Contact</a></li>
            </ul>
            @endauth
        </div>
    </div>
</footer>
