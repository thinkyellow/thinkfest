$(document).ready(function () {
    if ($("#myTable").length) {
        $("#myTable").DataTable();
    }
});

$(document).ready(function () {
    $("#test").on("change", function () {
        $("#range").html("Max aantal deelnemers: " + $(this).val());
        if ($(this).val() == 101) {
            $("#range").html("Max aantal deelnemers: " + "500");
        }
    });
});

$(".cd-schedule__events").on("scroll", function () {
    if ($(this).scrollLeft() === 0) {
        $(".cd-schedule__events").removeClass("hidden-before");
    } else {
        $(".cd-schedule__events").addClass("hidden-before");
    }
});

$(document).ready(function () {
    $("#myInput").on("keyup", function () {
        var filter, ul, location, em, i, title;
        filter = $("#myInput").val().toLowerCase();

        ul = $("#program");
        row = $(".column");
        activity = $(".activity");

        // set all activity on display none
        for (i = 0; i < activity.length; i++) {
            activity[i].parentNode.parentNode.style.display = "none";
        }

        // show only the activity that is being filtered
        for (i = 0; i < activity.length; i++) {
            em = activity[i].getElementsByTagName("em")[0];
            title = em.innerHTML.toLowerCase();
            startTime = em.parentNode.getAttribute("data-start");
            endTime = em.parentNode.getAttribute("data-end");
            location = document.getElementsByClassName("location-name");

            console.log(title);

            if (title.indexOf(filter) >= 0) {
                activity[i].parentNode.parentNode.style.display = "";
            } else if (startTime.indexOf(filter) >= 0) {
                activity[i].parentNode.parentNode.style.display = "";
            } else if (endTime.indexOf(filter) >= 0) {
                activity[i].parentNode.parentNode.style.display = "";
            }
        }
    });
});

$(document).ready(function () {
    $(".filter-tag").change(function () {
        var activity, tags, i;

        activity = $(".activity");
        tags = $(".tags");

        for (i = 0; i < activity.length; i++) {
            activity[i].style.display = "none";
            activity[i].parentNode.parentNode.style.display = "none";
        }

        if ($('input[type="checkbox"]:checked').length > 0) {
            $(".filter-tag")
                .filter(":checked")
                .each(function () {
                    for (i = 0; i < tags.length; i++) {
                        if (this.id.indexOf(tags[i].innerHTML) == 0) {
                            tags[i].parentNode.parentNode.style.display = "";
                            tags[
                                i
                            ].parentNode.parentNode.parentNode.parentNode.style.display =
                                "";
                        }
                    }
                });
        } else {
            for (i = 0; i < activity.length; i++) {
                activity[i].style.display = "";
                activity[i].parentNode.parentNode.style.display = "";
            }
        }
    });
});

$(".tag-list").on("click", function () {
    $("#removedTags").val($("#removedTags").val() + this.id + ",");
    this.remove();
});