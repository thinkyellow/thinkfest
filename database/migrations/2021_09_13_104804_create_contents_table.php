<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->string('page');
            $table->string('position');
            $table->longText('wysiwyg_nl');
            $table->longText('wysiwyg_en');
            $table->string('image_path')->nullable();
            $table->string('button_nl')->nullable();
            $table->string('button_en')->nullable();
            $table->string('link_nl')->nullable();
            $table->string('link_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
