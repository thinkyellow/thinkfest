<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('image_path');
            $table->text('description');
            $table->time('start_time');
            $table->time('end_time');
            $table->boolean('language');
            $table->integer('max_participants');
            $table->boolean('is_published')->default(false);
            $table->string('location_name');

            $table->foreignId('location_id')
                ->nullable()
                ->constrained()
                ->onUpdate('cascade');

            $table->foreignId('user_id')
                ->constrained()
                ->onUpdate('cascade');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
