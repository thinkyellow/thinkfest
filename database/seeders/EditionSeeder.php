<?php

namespace Database\Seeders;

use App\Models\Edition;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Edition::create(['name' => 'thinkfest_2021', 'is_active' => 1]);
    }
}
