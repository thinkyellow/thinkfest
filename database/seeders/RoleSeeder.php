<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['id' => 10, 'name' => 'Deelnemer']);
        Role::create(['id' => 20, 'name' => 'Organisator']);
        Role::create(['id' => 30, 'name' => 'Admin']);
    }
}
