<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        // if (Schema::connection("edition")->hasTable('editions')) {
        //     $edition = DB::connection('edition')->table('editions')
        //         ->where('is_active', 1)
        //         ->first();

        //     if (substr($edition->name, -2) == '22') {
        //         $username = env('DB_USERNAME_THREE');
        //         $password = env('DB_PASSWORD_THREE');
        //     } else {
        //         $username = env('DB_USERNAME_TWO');
        //         $password = env('DB_PASSWORD_TWO');
        //     }

        //     if ($edition) {
        //         DB::purge('mysql');
        //         Config::set("database.connections.mysql", [
        //             "driver" => "mysql",
        //             "host" => "127.0.0.1",
        //             "database" => $edition->name,
        //             "username" => $username,
        //             "password" => $password
        //         ]);
        //     } else {
        //         dd('Edition not set', 'fix this error display!');
        //     }
        // }
    }
}
