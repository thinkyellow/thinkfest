<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class IsDueMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ('2021-10-23 00:00:00' >= Carbon::now() && !in_array(auth()->user()->role_id, [Role::IS_ADMIN, Role::IS_ORGANIZER])) {
            abort('403');
        }

        return $next($request);
    }
}
