<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /**
     * Method login
     *
     * @return void
     */
    public function redirectToProvider()
    {
        return Socialite::with('graph')->redirect();
    }

    /**
     * Method callback
     *
     * @return void
     */
    public function handleProviderCallback()
    {
        $microsoftUser = Socialite::driver('graph')->stateless()->user();

        $user = User::firstOrCreate(
            [
                'provider_id' => $microsoftUser->getId(),
            ],
            [
                'email' => $microsoftUser->getEmail(),
                'name' => $microsoftUser->getName(),
                'role_id' => 20
            ]
        );

        $admins = ['a.a.m.plugge@hhs.nl', 'j.e.c.vanrosmalen@hhs.nl', 'h.i.buimer@hhs.nl', 'c.vanenst@hhs.nl'];

        if (in_array($user->email, $admins)) {
            User::where('provider_id', $microsoftUser->getId())->update(['role_id' => 30]);
        }

        auth()->login($user, true);

        return redirect('/nl/public/dashboard');
    }

    public function viewRegister()
    {
        return view('pages.auth.register');
    }

    public function register(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'max:255', 'min:8'],
        ]);

        User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => Hash::make($validated['password']),
            'role_id' => 20
        ]);

        return back()->with('message', 'Successfully added user');
    }
}
