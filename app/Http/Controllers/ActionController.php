<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Services\MailService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActionController extends Controller
{

    public function participate(Request $request)
    {

        $allActivity = Activity::with('participants')->has('participants')->get();

        foreach ($allActivity as $key => $activity) {
            $break = false;
            foreach ($activity->participants as $participant) {
                if ($participant->id == Auth::user()->id) {
                    $break = true;
                }
            }
            if (!$break) {
                unset($allActivity[$key]);
            }
        }

        $currentActivity = Activity::find($request->id);

        foreach ($allActivity as $activity) {
            if (
                strtotime($activity->start_time) <= strtotime($currentActivity->start_time) &&
                strtotime($activity->end_time) > strtotime($currentActivity->start_time) ||
                strtotime($activity->start_time) > strtotime($currentActivity->end_time) &&
                strtotime($activity->end_time) <= strtotime($currentActivity->end_time)
            ) {
                return back()->with('error', 'Unable to sign up due to a timing conflict with another activity.');
            } else if (
                strtotime($activity->start_time) >= strtotime($currentActivity->start_time) &&
                strtotime($activity->end_time) <= strtotime($currentActivity->end_time)
            ) { 
                return back()->with('error', 'Unable to sign up due to a timing conflict with another activity.');
            }
        }


        $currentActivity->participants()->attach(auth()->user()->id);

        MailService::participateGuest($request->id);

        return back()->with('message', 'Signed up successfully');
    }

    public function retire(Request $request)
    {
        $activity = Activity::find($request->id);
        $activity->participants()->detach(auth()->user()->id);

        MailService::retireGuest($request->id);

        return back()->with('message', 'Signed off successfully');
    }

    public function favorite(Request $request)
    {
        $activity = Activity::find($request->id);
        $activity->favorite()->attach(auth()->user()->id);

        return back()->with('message', 'Successfully added to favourites');
    }

    public function unfavorite(Request $request)
    {
        $activity = Activity::find($request->id);
        $activity->favorite()->detach(auth()->user()->id);

        return back()->with('message', 'Successfully removed from favourites');
    }
}
