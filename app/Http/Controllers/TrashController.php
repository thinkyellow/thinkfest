<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Http\Testing\File;

class TrashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trashed = Activity::onlyTrashed()->get();

        return view('pages.trash.list', compact('trashed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $activity = Activity::withTrashed()->find($id)->restore();

        return redirect('admin/trash/list')->with('message', 'Activiteit is hersteld.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::withTrashed()->find($id);

        unlink(public_path() . '/images/activities/' .$activity->image_path);

        $activity->forceDelete();

        return redirect('admin/trash/list')->with('message', 'Activiteit is verwijderd.');
    }
}
