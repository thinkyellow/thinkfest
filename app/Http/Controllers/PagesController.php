<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Content;
use App\Models\Faq;
use App\Models\Location;
use App\Models\Tag;
use App\Services\CountService;

class PagesController extends Controller
{
    public function adminDashboard()
    {
        $countArray = CountService::getNumbers();

        return view('pages.dashboard', compact('countArray'));
    }

    public function publicDashboard()
    {
        $countArray = CountService::getNumbers();

        $countArray = array_slice($countArray, 0, -5);

        $locations = Location::orderBy('room')
            ->has('activities', '>' , 0)
            ->orWhere('room', 'Atrium')
            ->with('activities.tags', 'activities.organizers', 'activities.location', 'activities.participants', 'activities.favorite')
            ->with(['activities' => function ($query) {
                $query->select('*')->where('is_published', 1);
            }])->get();

        $tags = Tag::all();

        $contents = Content::where("page", 'home')->get();

        return view('public.dashboard', compact('countArray', 'locations', 'tags', 'contents'));
    }

    /**
     * Method getFaq
     *
     * @return void
     */
    public function getFaq()
    {
        $faqs = Faq::all();

        return view('public.faq.view', compact('faqs'));
    }

    /**
     * Method getActivityList
     *
     * @return void
     */
    public function getActivityList()
    {
        $activities = Activity::with('tags')->paginate(10);

        return view('public.activity.list.view', compact('activities'));
    }

    /**
     * Method getActivityCalender
     *
     * @return void
     */
    public function getActivityCalender()
    {
        $activities = Activity::with('tags')->paginate(10);

        return view('public.activity.calender.view', compact('activities'));
    }

    /**
     * Method getAboutThinkFest
     *
     * @return void
     */
    public function getAboutThinkFest()
    {
        $contents = Content::where("page", 'about-think-fest')->get();

        return view('public.about-think-fest.view', compact('contents'));
    }

    /**
     * Method getProgram
     *
     * @return void
     */
    public function getProgram()
    {
        $locations = Location::with('activities.user', 'activities.participants')->get();

        return view('public.program.view', compact('locations'));
    }

    /**
     * Method getPressKit
     *
     * @return void
     */
    public function getPressKit()
    {
        $contents = Content::where("page", 'toolkit')->get();

        return view('public.press-kit.view', compact('contents'));
    }

    /**
     * Method getContact
     *
     * @return void
     */
    public function getContact()
    {
        $contents = Content::where("page", 'contact')->get();

        return view('public.contact.view', compact('contents'));
    }

    /**
     * Method getPrivacyStatement
     *
     * @return void
     */
    public function getPrivacyStatement()
    {
        $contents = Content::where("page", 'privacy')->get();

        return view('public.privacy-statement.view', compact('contents'));
    }

    /**
     * Method getCookies
     *
     * @return void
     */
    public function getCookies()
    {
        $contents = Content::where("page", 'cookies')->get();

        return view('public.cookies.view', compact('contents'));
    }

    /**
     * Method getCookies
     *
     * @return void
     */
    public function getEdNext()
    {
        $contents = Content::where("page", 'ednext')->get();

        return view('public.ednext.view', compact('contents'));
    }

}
