<?php

namespace App\Http\Controllers;

use App\Http\Requests\Edition\StoreEditionRequest;
use App\Http\Requests\Edition\EditEditionRequest;
use App\Models\Edition;
use Illuminate\Http\Request;

class EditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $editions = Edition::all();

        return view('pages.edition.list', compact('editions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.edition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEditionRequest $request)
    {
        $this->authorize('create', Edition::class);

        $request->request->add(['is_active' => 0]);

        $modifiedName = str_replace(' ', '_', mb_strtolower(trim($request->name)));

        $request->merge([
            'name' => $modifiedName,
        ]);

        Edition::create($request->all());

        return redirect('admin/edition/list')->with('message', 'Editie is aakt.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Edition  $edition
     * @return \Illuminate\Http\Response
     */
    public function show(Edition $edition)
    {
        return $edition;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Edition  $edition
     * @return \Illuminate\Http\Response
     */
    public function edit(Edition $edition)
    {
        return view('pages.edition.edit', compact('edition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Edition  $edition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Edition $edition)
    {
        $this->authorize('update', $edition);

        if (!isset($request->is_active)) {
            $request->request->add(['is_active' => 0]);
        }

        Edition::where('is_active', 1)->update(['is_active' => 0]);

        $edition->update($request->all());

        return back()->with('message', 'Edition changed.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Edition  $edition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Edition $edition)
    {
        $this->authorize('delete', Edition::class);

        $edition->delete();

        return back()->with('message', 'Edition removed.');
    }
}
