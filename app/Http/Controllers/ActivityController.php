<?php

namespace App\Http\Controllers;

use App\Http\Requests\Activity\EditActivityRequest;
use App\Models\Tag;
use App\Models\Activity;
use App\Models\Location;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == Role::IS_ADMIN) {
            $activities = Activity::where('is_published', 1)->with('tags', 'location', 'organizers')->get();
        } else {
            $invitedActivities = Activity::where('is_published', 1)->with('tags', 'location', 'organizers')->get();

            $activities = [];
            foreach ($invitedActivities as $activity) {
                if ($activity->organizers->contains(Auth::user()->id)) {
                    $activities[] = $activity;
                }
            }
        }

        return view('pages.activity.list', compact('activities'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        return $activity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity           = Activity::find($id);
        $locations          = Location::all();
        $tags               = Tag::all();
        $selectedTags       = Activity::where('id', $id)->get()->pluck('tags')->first();

        foreach ($tags as $tag) {
            foreach ($selectedTags as $selected) {
                if ($selected->id === $tag->id) {
                    $tag->selected = true;
                }
            }
        }

        return view('pages.activity.edit', compact('activity', 'locations', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(EditActivityRequest $request, Activity $activity)
    {
        $this->authorize('update', $activity);

        if (isset($request->image)) {
            $newImageName = time() . '-' . $request->location_id . '.' . $request->image->extension();
            $request->image->move(public_path('images/activities'), $newImageName);

            $request->merge([
                'image_path' => $newImageName
            ]);
        }

        $activity->tags()->detach();
        if (isset($request->tag)) {
            foreach ($request->tag as $tag) {
                $activity->tags()->attach($tag);
            }
        }

        $activity->update($request->all());

        return redirect('admin/activity/list')->with('message', 'Activity updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        $this->authorize('delete', $activity);

        $activity->delete();

        return back()->with('message', 'Activity removed. Check your mailbox for confirmation.');
    }
}
