<?php

namespace App\Http\Controllers;

use App\Http\Requests\Activity\EditDraftRequest;
use App\Http\Requests\Activity\StoreDraftRequest;
use App\Models\Activity;
use App\Models\Content;
use App\Models\Location;
use App\Models\Role;
use App\Models\Tag;
use App\Models\User;
use App\Services\MailService;
use Illuminate\Support\Facades\Auth;

class DraftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == Role::IS_ADMIN) {
            $drafts = Activity::where('is_published', 0)->with('tags', 'organizers', 'participants', 'location')->get();
        } else {
            $drafts = Activity::where('is_published', 0)->where('user_id', Auth::id())->with('tags', 'organizers', 'participants', 'location')->get();
        }

        return view('pages.draft.list', compact('drafts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Activity::class);

        $organizers = User::where('role_id', Role::IS_ORGANIZER)->orWhere('role_id', Role::IS_ADMIN)->get();
        $content    = Content::where('page', 'draft')->first();
        $locations  = Location::all();

        return view('pages.draft.create', compact('organizers', 'content', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDraftRequest $request)
    {
        $this->authorize('create', Activity::class);

        $newImageName = time() . '-' . $request->location_id . '.' . $request->image->extension();
        $request->image->move(public_path('images/activities'), $newImageName);

        $request->merge([
            'image_path' => $newImageName
        ]);

        $times = explode('|', $request->activity_time);

        $request->merge([
            'start_time' => $times[0],
            'end_time' => $times[1]
        ]);

        $activity = Activity::create($request->all());

        $activity->organizers()->attach($request->user_id);
        if ($request->organizer_id != null) {
            $activity->organizers()->attach($request->organizer_id);
        }
        $activity->tags()->attach($request->tags);

        MailService::conceptCreate($activity->id);

        return redirect('admin/draft/list')->with('message', 'Activity created! Check your mailbox for confirmation');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        return $activity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity           = Activity::find($id);
        $locations          = Location::all();
        $organizers         = User::where('role_id', Role::IS_ORGANIZER)->orWhere('role_id', Role::IS_ADMIN)->get();
        $selectedOrganizers = Activity::where('id', $id)->get()->pluck('organizers')->first();
        $tags               = Tag::all();
        $selectedTags       = Activity::where('id', $id)->get()->pluck('tags')->first();
        $locationActivities = Activity::where('location_id', $activity->location_id)->get();
        $name               = User::find($activity->user_id)->name;

        foreach ($organizers as $organizer) {
            foreach ($selectedOrganizers as $selected) {
                if ($selected->id === $organizer->id) {
                    $organizer->selected = true;
                }
            }
        }

        foreach ($tags as $tag) {
            foreach ($selectedTags as $selected) {
                if ($selected->id === $tag->id) {
                    $tag->selected = true;
                }
            }
        }
        
        return view('pages.draft.edit', compact('activity', 'locations', 'organizers', 'tags', 'locationActivities', 'name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(EditDraftRequest $request, Activity $activity)
    {
        $this->authorize('update', $activity);

        if (isset($request->image)) {
            $newImageName = time() . '-' . $request->location_id . '.' . $request->image->extension();
            $request->image->move(public_path('images/activities'), $newImageName);

            $request->merge([
                'image_path' => $newImageName
            ]);
        }

        $activity->tags()->detach();
        if(isset($request->tag)) {
            foreach ($request->tag as $tag) {
                $activity->tags()->attach($tag);
            }
        }

        $activity->organizers()->detach();
        if(isset($request->organizer_id)) {
            foreach ($request->organizer_id as $organizer) {
                $activity->organizers()->attach($organizer);
            }
        }

        if ($request->user_id != null) {
            $activity->organizers()->detach($activity->user_id);
            $activity->organizers()->attach($request->user_id);
        }

        if ($request->is_published == true) {
            MailService::conceptToActivity($activity->id);
        }

        $activity->update($request->all());

        return redirect('admin/draft/list')->with('message', 'Activity updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        $this->authorize('delete', $activity);

        $activity->delete();

        return back()->with('message', 'Activity removed. Check your mailbox for confirmation');
    }
}
