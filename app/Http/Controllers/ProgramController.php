<?php

namespace App\Http\Controllers;

use App\Models\activity;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listParticipants($id)
    {
        $participants = Activity::where('id', $id)->with('participants')->get()->pluck('participants')[0];
        $activity = Activity::where('id', $id)->first();

        return view('pages.participant.list', compact('participants', 'activity', 'id'));
    }

    public function editParticipants($id)
    {
        $users = User::all();
        $selectedUsers = Activity::where('id', $id)->with('participants')->get()->pluck('participants')[0];

        foreach ($users as $user) {
            foreach ($selectedUsers as $selected) {
                if ($selected->id === $user->id) {
                    $user->selected = true;
                }
            }
        }

        return view('pages.participant.edit', compact('users', 'id'));
    }

    public function addParticipants(Request $request)
    {
        $activity = Activity::find($request->activityId);

        $activity->participants()->detach();

        if (isset($request->users)) {
            foreach ($request->users as $user) {
                $activity->participants()->attach($user);
            }
        }

        return $this->listParticipants($request->activityId);
    }

    public function retireParticipant(Request $request)
    {
        $activity = Activity::find($request->activityId);
        $activity->participants()->detach($request->participantId);

        return $this->listParticipants($request->activityId);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listProgram()
    {
        $invitedActivities = Activity::where('is_published', 1)->with('tags', 'location', 'participants')->get();

        $activities = [];
        foreach ($invitedActivities as $activity) {
            if ($activity->participants->contains(Auth::user()->id)) {
                $activities[] = $activity;
            }
        }

        return view('pages.program.list', compact('activities'));
    }

    public function listFavorite()
    {
        $invitedActivities = Activity::where('is_published', 1)->with('tags', 'location', 'favorite')->get();

        $activities = [];
        foreach ($invitedActivities as $activity) {
            if ($activity->favorite->contains(Auth::user()->id)) {
                $activities[] = $activity;
            }
        }

        return view('pages.favorite.list', compact('activities'));
    }
}
