<?php

namespace App\Http\Controllers;

use App\Http\Requests\Content\StoreContentRequest;
use App\Http\Requests\Content\EditContentRequest;
use App\Models\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = Content::all();

        return view('pages.content.list', compact('contents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContentRequest $request)
    {
        if ($request->image != null) {
            $newImageName = time() . '-' . $request->page . '.' . $request->image->extension();
            $request->image->move(public_path('images/activities'), $newImageName);

            $request->merge([
                'image_path' => $newImageName
            ]);
        }

        Content::create($request->all());

        return redirect('admin/content/list')->with('message', 'Activity created! Check your mailbox for confirmation');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content)
    {
        $content = $content;

        return view('pages.content.edit', compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(EditContentRequest $request, Content $content)
    {
        if ($request->image != null) {
            $newImageName = time() . '-' . $request->page . '.' . $request->image->extension();
            $request->image->move(public_path('images/activities'), $newImageName);

            $request->merge([
                'image_path' => $newImageName
            ]);
        }

        $content->update($request->all());

        return redirect('admin/content/list')->with('message', 'Content updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $content)
    {
        $content->delete();

        return back()->with('message', 'Content is verwijderd.');
    }
}
