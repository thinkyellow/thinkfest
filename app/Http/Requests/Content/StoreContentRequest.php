<?php

namespace App\Http\Requests\Content;

use Illuminate\Foundation\Http\FormRequest;

class StoreContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wysiwyg_nl' => 'required',
            'wysiwyg_en' => 'nullable',
            'button_nl' => 'nullable',
            'button_en' => 'nullable',
            'link_nl' => 'nullable',
            'link_en' => 'nullable',
            'image' => 'nullable',
            'page' => 'required',
            'position' => 'required'
        ];
    }
}
