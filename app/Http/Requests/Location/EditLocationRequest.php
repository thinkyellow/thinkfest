<?php

namespace App\Http\Requests\Location;

use Illuminate\Foundation\Http\FormRequest;

class EditLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room' => 'required',
            'location' => 'required',
            'image' => 'required',
            'location_description' => 'required',
            'subtext' => 'required',
            'max_possible_participants' => 'required|integer'
        ];
    }
}
