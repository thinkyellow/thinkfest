<?php

namespace App\Http\Requests\Activity;

use Illuminate\Foundation\Http\FormRequest;

class StoreDraftRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'max_participants' => 'required',
            'language' => 'required',
            'organizer_id' => 'nullable',
            'start_time' => 'nullable',
            'end_time' => 'nullable',
            'image' => 'required',
            'location_name' => 'required'
        ];
    }
}
