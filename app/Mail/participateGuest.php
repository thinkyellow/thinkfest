<?php

namespace App\Mail;

use App\Models\Activity;
use App\Models\Location;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class participateGuest extends Mailable
{
    use Queueable, SerializesModels;

    public $activity;
    public $location;
    public $organizer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($activityId)
    {
        $this->activity = Activity::find($activityId);
        $this->location = Location::find($this->activity->location_id);
        $allOrganizers = Activity::where('id', $activityId)->with('organizers')->first();

        foreach($allOrganizers->organizers as $organizer) {
            if ($organizer->id == $this->activity->user_id) {
                $this->organizer = $organizer;
            }
        } 
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email/participateGuest');
    }
}
