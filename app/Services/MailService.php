<?php

namespace App\Services;

use App\Mail\activityDeleteMail;
use App\Mail\conceptToActivity;
use App\Mail\createConceptMail;
use App\Mail\participateGuest;
use App\Mail\retireGuest;
use App\Models\Activity;
use Illuminate\Support\Facades\Mail;

class MailService
{
    public static function conceptCreate($attributes)
    {
        Mail::to(auth()->user()->email)->send(new createConceptMail($attributes));
        Mail::to('thinkfest@hhs.nl')->send(new createConceptMail($attributes));
    }

    public static function conceptToActivity($attributes)
    {
        $object = Activity::find($attributes)->with('organizers')->get()[0];

        Mail::to($object->organizers[0]->email)->send(new conceptToActivity($attributes));
        Mail::to('thinkfest@hhs.nl')->send(new conceptToActivity($attributes));
    }

    public static function activityDelete($attributes)
    {
        $object = Activity::find($attributes)->with('organizers')->get()[0];

        Mail::to($object->organizers[0]->email)->send(new activityDeleteMail($attributes));
        Mail::to('thinkfest@hhs.nl')->send(new createConceptMail($attributes));
    }

    public static function participateGuest($attributes)
    {
        Mail::to(auth()->user()->email)->send(new participateGuest($attributes));
        Mail::to('thinkfest@hhs.nl')->send(new participateGuest($attributes));
    }

    public static function retireGuest($attributes)
    {
        Mail::to(auth()->user()->email)->send(new retireGuest($attributes));
        Mail::to('thinkfest@hhs.nl')->send(new retireGuest($attributes));
    }
}
