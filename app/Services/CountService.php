<?php

namespace App\Services;

use App\Models\Activity;
use App\Models\FAQ;
use App\Models\Location;
use App\Models\Tag;
use App\Models\User;

class CountService {

    public static function getNumbers()
    {
        $countArray = [];

        $countArray['concepts'] = count(Activity::where('is_published', 0)->get('id'));
        $countArray['activities'] = count(Activity::where('is_published', 1)->get('id'));
        $countArray['admin'] = count(User::where('role_id', 30)->get('id'));
        $countArray['organizers'] = count(User::where('role_id', 20)->get('id'));
        $countArray['users'] = count(User::all('id'));
        $countArray['whitelist'] = count(User::where('password', '!=', null)->get('id'));
        $countArray['locations'] = count(Location::all('id'));
        $countArray['FAQ'] = count(FAQ::all('id'));
        $countArray['tags'] = count(Tag::all('id'));

        return $countArray;
    }

}
