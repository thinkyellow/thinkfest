<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'name'];

    public const IS_USER = 10;
    public const IS_ORGANIZER = 20;
    public const IS_ADMIN = 30;
}
