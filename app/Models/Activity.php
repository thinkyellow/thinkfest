<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'image_path',
        'description',
        'start_time',
        'end_time',
        'language',
        'max_participants',
        'location_name',
        'location_id',
        'user_id',
        'is_published'
    ];

    
    public function location()
    {
        return $this->belongsTo(Location::class);
    }
    
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function organizers()
    {
        return $this->belongsToMany(User::class, 'activity_user');
    }

    public function participants()
    {
        return $this->belongsToMany(User::class, 'activity_participants');
    }

    public function favorite()
    {
        return $this->belongsToMany(User::class, 'activity_favorites');
    }
}
