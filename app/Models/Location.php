<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $fillable = [
        'room',
        'location',
        'image',
        'location_description',
        'subtext',
        'max_possible_participants'
    ];

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }
}
