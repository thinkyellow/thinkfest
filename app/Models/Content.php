<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use HasFactory;

    protected $fillable = [
        'page',
        'position',
        'wysiwyg_nl',
        'wysiwyg_en',
        'image_path',
        'button_nl',
        'button_en',
        'link_nl',
        'link_en'
    ];
}