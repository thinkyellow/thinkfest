<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = ['title_nl', 'title_en', 'color'];

    public function activities()
    {
        return $this->belongsToMany(Activity::class);
    }
}
